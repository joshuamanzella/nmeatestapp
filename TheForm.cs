﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using NMEA_Utils;
using System.Diagnostics;

namespace NMEA_TestApp
{
    public partial class TheForm : Form
    {
        public TheForm()
        {
            InitializeComponent();

            mLogFile = new StreamWriter(Environment.CurrentDirectory + "\\NMEA_TestApp.log");
            mNMEA_Handler = new NMEA_Handler();
            mLock = new object();
            mByteCounter = 0;
            mTotalPackets = 0;
            mUnknownPackets = 0;
            mCaptureTimer = new Stopwatch();
            mTimeSinceLastRx = new Stopwatch();

            // Set up our error handling
            NMEA_Utils.ErrorHandler.Instance().NMEA_ErrorEvent += HandleNMEA_ErrorEvent;

            // Initialize the satellite grid on the GSV tab
            for (int row = 1; row < GsvSatellitesTableLayoutPanel.RowCount; row++)
            {
                // Go through each column
                for (int col = 1; col < GsvSatellitesTableLayoutPanel.ColumnCount; col++)
                {
                    Label label = new Label();
                    label.TextAlign = ContentAlignment.MiddleCenter;
                    GsvSatellitesTableLayoutPanel.Controls.Add(label, col, row);
                }
            }

            GuiTimer.Start();
        }

        void HandleNMEA_ErrorEvent(object sender, NMEA_ErrorEventArgs e)
        {
            mLogFile.WriteLine(e.Message);
        }

        private void MySerialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (!mTimeSinceLastRx.IsRunning)
            {
                mTimeSinceLastRx.Start();
            }
            else
            {
                mTimeSinceLastRx.Restart();
            }

            // We got a packet
            mTotalPackets++;

            // Get the data from the serial port
            string raw = "";

            try
            {
                raw = MySerialPort.ReadLine();
            }
            catch (Exception exception)
            {
                mLogFile.WriteLine(exception.Message);
                return;
            }

            // Track the number of characters received
            mByteCounter += raw.Length;

            // Check that we actually have enough data to parse (at least the header)
            if (raw.Length < 3)
            {
                return;
            }

            if (raw[0].Equals('$'))
            {
                // Check for valid NMEA sentence which should start with '$GP'
                if (raw.Substring(0, 3).Equals("$GP"))
                {
                    mNMEA_Handler.ParseSentence(raw);
                }
                else
                {
                    mUnknownPackets++;
                    mLogFile.WriteLine("Unknown GPS Data: " + raw);
                }
            }
            else
            {
                mUnknownPackets++;
                mLogFile.WriteLine("Unknown Packet: " + raw);
            }
        }

        private void SerialPortSelectButton_DropDownOpening(object sender, EventArgs e)
        {
            // Get the toolstrip items from the sender
            ToolStripItemCollection items = ((ToolStripDropDownButton)sender).DropDown.Items;

            // Clear the drop-down
            items.Clear();

            // First, add a "Disconnect" option if the port is currently open
            if (MySerialPort.IsOpen)
            {
                items.Add("Disconnect");
            }

            // Build the drop-down list
            foreach (string port in SerialPort.GetPortNames())
            {
                // Add the port name as an item and disable it if we're already connected
                items.Add(port).Enabled = !(MySerialPort.IsOpen && MySerialPort.PortName.Equals(port));
            }

            // Add "None" to the list to indicate there is no available COM port
            if (items.Count == 0)
            {
                items.Add("None").Enabled = false;
            }
        }

        private void SerialPortSelectButton_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Text.Equals("Disconnect"))
            {
                MySerialPort.Close();
                MySerialPort.PortName = "NONE";

                mCaptureTimer.Stop();
                mTimeSinceLastRx.Stop();
            }
            else
            {
                MySerialPort.PortName = e.ClickedItem.Text;
                MySerialPort.Open();

                mCaptureTimer.Restart();
                mTimeSinceLastRx.Reset();
            }
        }

        private void GuiTimer_Tick(object sender, EventArgs e)
        {
            lock (mLock)
            {
                string position = "";

                // Position display
                if (GgaPositionRadioButton.Checked)
                {
                    mPositionDisplayLatitude_deg = mNMEA_Handler.GGA.LatitudeD;
                    mPositionDisplayLongitude_deg = mNMEA_Handler.GGA.LongitudeD;

                    position = mNMEA_Handler.GGA.LatitudeDMS + " / " + mNMEA_Handler.GGA.LongitudeDMS + "\r\n";
                    position += mNMEA_Handler.GGA.LatitudeDM + " / " + mNMEA_Handler.GGA.LongitudeDM + "\r\n";
                    position += mNMEA_Handler.GGA.LatitudeD + " / " + mNMEA_Handler.GGA.LongitudeD + "\r\n";
                }
                else if (GllPositionRadioButton.Checked)
                {
                    mPositionDisplayLatitude_deg = mNMEA_Handler.GLL.LatitudeD;
                    mPositionDisplayLongitude_deg = mNMEA_Handler.GLL.LongitudeD;

                    position = mNMEA_Handler.GLL.LatitudeDMS + " / " + mNMEA_Handler.GLL.LongitudeDMS + "\r\n";
                    position += mNMEA_Handler.GLL.LatitudeDM + " / " + mNMEA_Handler.GLL.LongitudeDM + "\r\n";
                    position += mNMEA_Handler.GLL.LatitudeD + " / " + mNMEA_Handler.GLL.LongitudeD + "\r\n";
                }
                else if (RmcPositionRadioButton.Checked)
                {
                    mPositionDisplayLatitude_deg = mNMEA_Handler.RMC.LatitudeD;
                    mPositionDisplayLongitude_deg = mNMEA_Handler.RMC.LongitudeD;

                    position = mNMEA_Handler.RMC.LatitudeDMS + " / " + mNMEA_Handler.RMC.LongitudeDMS + "\r\n";
                    position += mNMEA_Handler.RMC.LatitudeDM + " / " + mNMEA_Handler.RMC.LongitudeDM + "\r\n";
                    position += mNMEA_Handler.RMC.LatitudeD + " / " + mNMEA_Handler.RMC.LongitudeD + "\r\n";
                }
                else
                {
                    position = "Da Fuq?";
                }

                GpsPositionTextBox.Text = position;

                // Counters
                GgaPacketCounterTextBox.Text = mNMEA_Handler.GGA.ReceivedPackets.ToString();
                GllPacketCounterTextBox.Text = mNMEA_Handler.GLL.ReceivedPackets.ToString();
                GsaPacketCounterTextBox.Text = mNMEA_Handler.GSA.ReceivedPackets.ToString();
                GsvPacketCounterTextBox.Text = mNMEA_Handler.GSV.ReceivedPackets.ToString();
                RmcPacketCounterTextBox.Text = mNMEA_Handler.RMC.ReceivedPackets.ToString();
                VtgPacketCounterTextBox.Text = mNMEA_Handler.VTG.ReceivedPackets.ToString();

                UnknownPacketCounterTextBox.Text = mUnknownPackets.ToString();
                ToTalPacketsTextBox.Text = mTotalPackets.ToString();
                TotalBytesTextBox.Text = mByteCounter.ToString();

                CaptureTimeTextBox.Text = mCaptureTimer.Elapsed.ToString();
                TimeSinceLastRxTextBox.Text = mTimeSinceLastRx.Elapsed.ToString();

                // GGA
                DateTime utcTime;
                if (DateTime.TryParse(mNMEA_Handler.GGA.UtcTime, out utcTime))
                {
                    UtcTimeTextBox.Text =  utcTime.ToString();
                }
                else
                {
                    UtcTimeTextBox.Text = "Not Available";
                }

                PositionTextBox.Text = mNMEA_Handler.GGA.LatitudeD + " / " + mNMEA_Handler.GGA.LongitudeD;
                GpsQualityTextBox.Text = mNMEA_Handler.GGA.GpsQuality;
                SatellitesUsedTextBox.Text = mNMEA_Handler.GGA.SatellitesUsed.ToString();
                HorizontalDopTextBox.Text = mNMEA_Handler.GGA.HDOP.ToString();
                AltitudeTextBox.Text = mNMEA_Handler.GGA.Altitude_Ft.ToString();
                ChecksumTextBox.Text = mNMEA_Handler.GGA.Checksum;

                // GLL
                GllUtcTimeTextBox.Text = mNMEA_Handler.GLL.UtcTime;
                GllPositionTextBox.Text = mNMEA_Handler.GLL.LatitudeD + " / " + mNMEA_Handler.GLL.LongitudeD;
                GllStatusTextBox.Text = mNMEA_Handler.GLL.Status;
                GllModeIndicatorTextBox.Text = mNMEA_Handler.GLL.ModeIndicator;
                GllChecksumTextBox.Text = mNMEA_Handler.GLL.Checksum;

                // GSA
                GsaFixModeTextBox.Text = mNMEA_Handler.GSA.FixMode;
                GsaFixTypeTextBox.Text = mNMEA_Handler.GSA.FixType;

                string satsUsed = "";

                foreach (string sat in mNMEA_Handler.GSA.SatellitesUsed)
                {
                    if (sat != null)
                    {
                        satsUsed += (sat + ",");
                    }
                }

                GsaSatellitesUsedTextBox.Text = satsUsed;

                GsaPositionDopTextBox.Text = mNMEA_Handler.GSA.PDOP.ToString();
                GsaHorizontalDopTextBox.Text = mNMEA_Handler.GSA.HDOP.ToString();
                GsaVerticalDopTextBox.Text = mNMEA_Handler.GSA.VDOP.ToString();
                GsaChecksumTextBox.Text = mNMEA_Handler.GSA.Checksum;

                // GSV
                NMEA_SentenceGSV.sSat[] sats = mNMEA_Handler.GSV.VisibleSatellites;

                for (int row = 1; row < GsvSatellitesTableLayoutPanel.RowCount; row++)
                {
                    GsvSatellitesTableLayoutPanel.GetControlFromPosition(1, row).Text = sats[row - 1].mID.ToString();
                    GsvSatellitesTableLayoutPanel.GetControlFromPosition(2, row).Text = sats[row - 1].mElevation_deg.ToString() + "°";
                    GsvSatellitesTableLayoutPanel.GetControlFromPosition(3, row).Text = sats[row - 1].mAzimuth_deg.ToString() + "°";
                    GsvSatellitesTableLayoutPanel.GetControlFromPosition(4, row).Text = sats[row - 1].mSNR.ToString();
                }

                GsvDataTableLayoutPanel.GetControlFromPosition(0, 1).Text = mNMEA_Handler.GSV.TotalMessages.ToString();
                GsvDataTableLayoutPanel.GetControlFromPosition(0, 3).Text = mNMEA_Handler.GSV.SequenceNumber.ToString();
                GsvDataTableLayoutPanel.GetControlFromPosition(0, 5).Text = mNMEA_Handler.GSV.SatellitesInView.ToString();

                // RMC
                RmcUtcDateTimeTextBox.Text = mNMEA_Handler.RMC.UtcDateOfPosFix + " " + mNMEA_Handler.RMC.UtcTime.ToString();
                RmcPositionTextBox.Text = mNMEA_Handler.RMC.LatitudeD + " / " + mNMEA_Handler.RMC.LongitudeD;
                RmcStatusTextBox.Text = mNMEA_Handler.RMC.Status;
                RmcModeIndicatorTextBox.Text = mNMEA_Handler.RMC.ModeIndicator;
                RmcGroundSpeedTextBox.Text = mNMEA_Handler.RMC.GroundSpeed_Knots.ToString() + " kts";
                RmcTrackAngleTextBox.Text = mNMEA_Handler.RMC.TrackAngle_Deg.ToString() + (char)176;
                RmcChecksumTextBox.Text = mNMEA_Handler.RMC.Checksum;

                // VTG
                VtgPlaceholderTextBox.Text = "Placeholder";
                VtgMagneticCourseTextBox.Text = mNMEA_Handler.VTG.MagneticCourse_Deg.ToString() + (char)176;
                VtgGroundSpeedKtsTextBox.Text = mNMEA_Handler.VTG.GroundSpeed_Knots.ToString() + " kts";
                VtgModeIndicatorTextBox.Text = mNMEA_Handler.VTG.ModeIndicator;
                VtgGroundSpeedKphTextBox.Text = mNMEA_Handler.VTG.GroundSpeed_Kph.ToString() + " kph";
                VtgTrueCourseTextBox.Text = mNMEA_Handler.VTG.TrueCourse_Deg.ToString() + (char)176;
                VtgChecksumTextBox.Text = mNMEA_Handler.VTG.Checksum;

                if (MySerialPort.IsOpen)
                {
                    SerialPortStatusLabel.Text = "Connected to " + MySerialPort.PortName;
                }
                else
                {
                    SerialPortStatusLabel.Text = "Disconnected";
                }
            }
        }

        private void ResetStatsButton_Click(object sender, EventArgs e)
        {
            mNMEA_Handler.GGA.ReceivedPackets = 0;
            mNMEA_Handler.GLL.ReceivedPackets = 0;
            mNMEA_Handler.GSA.ReceivedPackets = 0;
            mNMEA_Handler.GSV.ReceivedPackets = 0;
            mNMEA_Handler.RMC.ReceivedPackets = 0;
            mNMEA_Handler.VTG.ReceivedPackets = 0;
            mUnknownPackets = 0;
            mTotalPackets = 0;
            mByteCounter = 0;

            if (MySerialPort.IsOpen)
            {
                mCaptureTimer.Restart();
            }
            else
            {
                mCaptureTimer.Reset();
            }

            mTimeSinceLastRx.Reset();
        }

        private void PositionMapItButton_Click(object sender, EventArgs e)
        {
            // Create a google URL of the form https://maps.google.com/maps?q=58+41.881N+152+31.324W
            string target = "https://maps.google.com/maps?q=" + mPositionDisplayLatitude_deg + "+" + mPositionDisplayLongitude_deg;

            try
            {
                System.Diagnostics.Process.Start(target);
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                    MessageBox.Show(noBrowser.Message);
            }
            catch (System.Exception other)
            {
                MessageBox.Show(other.Message);
            }
        }

        private StreamWriter mLogFile;
        private NMEA_Utils.NMEA_Handler mNMEA_Handler;
        private object mLock;
        private int mByteCounter;
        private int mTotalPackets;
        private int mUnknownPackets;
        private Stopwatch mCaptureTimer;
        private Stopwatch mTimeSinceLastRx;
        private string mPositionDisplayLatitude_deg;
        private string mPositionDisplayLongitude_deg;

        private void SerialMonitorButton_Click(object sender, EventArgs e)
        {
            // Create and display the serial monitor window
        }
    }
}
