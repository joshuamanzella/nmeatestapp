﻿namespace NMEA_TestApp
{
    partial class TheForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TheForm));
            this.MySerialPort = new System.IO.Ports.SerialPort(this.components);
            this.MainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.MainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.SerialPortStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.MainToolStrip = new System.Windows.Forms.ToolStrip();
            this.SerialPortSelectButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.SerialMonitorButton = new System.Windows.Forms.ToolStripButton();
            this.NmeaSentenceTabControl = new System.Windows.Forms.TabControl();
            this.GgaPage = new System.Windows.Forms.TabPage();
            this.GgaTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.UtcTimeTextBox = new System.Windows.Forms.TextBox();
            this.GgaPositionLabel = new System.Windows.Forms.Label();
            this.PositionTextBox = new System.Windows.Forms.TextBox();
            this.GgaGpsQualityLabel = new System.Windows.Forms.Label();
            this.GgaSatellitesUsedLabel = new System.Windows.Forms.Label();
            this.GgaHdopLabel = new System.Windows.Forms.Label();
            this.GgaAltitudeLabel = new System.Windows.Forms.Label();
            this.GpsQualityTextBox = new System.Windows.Forms.TextBox();
            this.SatellitesUsedTextBox = new System.Windows.Forms.TextBox();
            this.HorizontalDopTextBox = new System.Windows.Forms.TextBox();
            this.AltitudeTextBox = new System.Windows.Forms.TextBox();
            this.ChecksumTextBox = new System.Windows.Forms.TextBox();
            this.GgaChecksumLabel = new System.Windows.Forms.Label();
            this.GgaUtcTimeLabel = new System.Windows.Forms.Label();
            this.GllPage = new System.Windows.Forms.TabPage();
            this.GllTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.GllStatusLabel = new System.Windows.Forms.Label();
            this.GllModeIndicatorLabel = new System.Windows.Forms.Label();
            this.GllChecksumLabel = new System.Windows.Forms.Label();
            this.GllUtcTimeTextBox = new System.Windows.Forms.TextBox();
            this.GllPositionTextBox = new System.Windows.Forms.TextBox();
            this.GllStatusTextBox = new System.Windows.Forms.TextBox();
            this.GllModeIndicatorTextBox = new System.Windows.Forms.TextBox();
            this.GllChecksumTextBox = new System.Windows.Forms.TextBox();
            this.GllUtcLabel = new System.Windows.Forms.Label();
            this.PositionLabel = new System.Windows.Forms.Label();
            this.GsaPage = new System.Windows.Forms.TabPage();
            this.GsaTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.GsaFixModeLabel = new System.Windows.Forms.Label();
            this.GsaFixTypeLabel = new System.Windows.Forms.Label();
            this.GsaSatellitesUsedLabel = new System.Windows.Forms.Label();
            this.GsaPositionDopLabel = new System.Windows.Forms.Label();
            this.GsaHorizontalDopLabel = new System.Windows.Forms.Label();
            this.GsaVerticalDopLabel = new System.Windows.Forms.Label();
            this.GsaFixModeTextBox = new System.Windows.Forms.TextBox();
            this.GsaFixTypeTextBox = new System.Windows.Forms.TextBox();
            this.GsaSatellitesUsedTextBox = new System.Windows.Forms.TextBox();
            this.GsaPositionDopTextBox = new System.Windows.Forms.TextBox();
            this.GsaHorizontalDopTextBox = new System.Windows.Forms.TextBox();
            this.GsaVerticalDopTextBox = new System.Windows.Forms.TextBox();
            this.GsaChecksumTextBox = new System.Windows.Forms.TextBox();
            this.GsaChecksumLabel = new System.Windows.Forms.Label();
            this.GsvPage = new System.Windows.Forms.TabPage();
            this.GsvTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.GsvDataTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.GsvTotalMessagesLabel = new System.Windows.Forms.Label();
            this.GsvSatellitesInViewLabel = new System.Windows.Forms.Label();
            this.GsvSequenceNumberLabel = new System.Windows.Forms.Label();
            this.GsvMessageCountText = new System.Windows.Forms.Label();
            this.GsvSequenceNumberText = new System.Windows.Forms.Label();
            this.GsvSatellitesNumberText = new System.Windows.Forms.Label();
            this.GsvSatellitesTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.GsvSatelliteLabel1 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel2 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel3 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel4 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel5 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel6 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel7 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel8 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel9 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel10 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel11 = new System.Windows.Forms.Label();
            this.GsvSatelliteLabel12 = new System.Windows.Forms.Label();
            this.GsvIdLabel = new System.Windows.Forms.Label();
            this.GsvElevationLabel = new System.Windows.Forms.Label();
            this.GsvAzimuthLabel = new System.Windows.Forms.Label();
            this.GsvSnrLabel = new System.Windows.Forms.Label();
            this.GsvSatelliteIndex = new System.Windows.Forms.Label();
            this.RmcPage = new System.Windows.Forms.TabPage();
            this.RmcTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.RmcUtcTimeLabel = new System.Windows.Forms.Label();
            this.RmcChecksumLabel = new System.Windows.Forms.Label();
            this.RmcUtcDateTimeTextBox = new System.Windows.Forms.TextBox();
            this.RmcPositionTextBox = new System.Windows.Forms.TextBox();
            this.RmcStatusTextBox = new System.Windows.Forms.TextBox();
            this.RmcModeIndicatorTextBox = new System.Windows.Forms.TextBox();
            this.RmcGroundSpeedTextBox = new System.Windows.Forms.TextBox();
            this.RmcTrackAngleTextBox = new System.Windows.Forms.TextBox();
            this.RmcChecksumTextBox = new System.Windows.Forms.TextBox();
            this.RmcStatusLabel = new System.Windows.Forms.Label();
            this.RmcPositionLabel = new System.Windows.Forms.Label();
            this.RmcGroundSpeedLabel = new System.Windows.Forms.Label();
            this.RmcModeIndicatorLabel = new System.Windows.Forms.Label();
            this.RcmTrackAngleLabel = new System.Windows.Forms.Label();
            this.VtgPage = new System.Windows.Forms.TabPage();
            this.VtgTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.VtgGroundSpeedKnotsLabel = new System.Windows.Forms.Label();
            this.VtgPlaceholderTextBox = new System.Windows.Forms.TextBox();
            this.VtgGroundSpeedKtsTextBox = new System.Windows.Forms.TextBox();
            this.VtgModeIndicatorTextBox = new System.Windows.Forms.TextBox();
            this.VtgGroundSpeedKphTextBox = new System.Windows.Forms.TextBox();
            this.VtgTrueCourseTextBox = new System.Windows.Forms.TextBox();
            this.VtgMagneticCourseLabel = new System.Windows.Forms.Label();
            this.VtgMagneticCourseTextBox = new System.Windows.Forms.TextBox();
            this.GvtModeIndicatorLabel = new System.Windows.Forms.Label();
            this.VtgGroundSpeedKphLabel = new System.Windows.Forms.Label();
            this.Checksum = new System.Windows.Forms.Label();
            this.VtgTrueCourseLabel = new System.Windows.Forms.Label();
            this.VtgChecksumTextBox = new System.Windows.Forms.TextBox();
            this.VtgPlaceholderLabel = new System.Windows.Forms.Label();
            this.GeneralStatusGroupBox = new System.Windows.Forms.GroupBox();
            this.CounterGroupBox = new System.Windows.Forms.GroupBox();
            this.CountersTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.GgaPacketCounterLabel = new System.Windows.Forms.Label();
            this.GgaPacketCounterTextBox = new System.Windows.Forms.TextBox();
            this.GllPacketCounterLable = new System.Windows.Forms.Label();
            this.GsaPacketCounterLabel = new System.Windows.Forms.Label();
            this.GsvPacketCounterLabel = new System.Windows.Forms.Label();
            this.RmcPacketCounterLabel = new System.Windows.Forms.Label();
            this.VtgPacketCounterLabel = new System.Windows.Forms.Label();
            this.GllPacketCounterTextBox = new System.Windows.Forms.TextBox();
            this.GsaPacketCounterTextBox = new System.Windows.Forms.TextBox();
            this.GsvPacketCounterTextBox = new System.Windows.Forms.TextBox();
            this.RmcPacketCounterTextBox = new System.Windows.Forms.TextBox();
            this.VtgPacketCounterTextBox = new System.Windows.Forms.TextBox();
            this.TotalPacketCounterLabel = new System.Windows.Forms.Label();
            this.TotalBytesCounterLabel = new System.Windows.Forms.Label();
            this.UnknownPacketCounterLabel = new System.Windows.Forms.Label();
            this.CaptureTimeLabel = new System.Windows.Forms.Label();
            this.UnknownPacketCounterTextBox = new System.Windows.Forms.TextBox();
            this.ToTalPacketsTextBox = new System.Windows.Forms.TextBox();
            this.TotalBytesTextBox = new System.Windows.Forms.TextBox();
            this.CaptureTimeTextBox = new System.Windows.Forms.TextBox();
            this.ResetStatsButton = new System.Windows.Forms.Button();
            this.TimeSinceLastRx = new System.Windows.Forms.Label();
            this.TimeSinceLastRxTextBox = new System.Windows.Forms.TextBox();
            this.GpsPositionGroupBox = new System.Windows.Forms.GroupBox();
            this.PositionMapItButton = new System.Windows.Forms.Button();
            this.PositionSourceSelectLabel = new System.Windows.Forms.Label();
            this.RmcPositionRadioButton = new System.Windows.Forms.RadioButton();
            this.GpsPositionTextBox = new System.Windows.Forms.TextBox();
            this.GllPositionRadioButton = new System.Windows.Forms.RadioButton();
            this.GgaPositionRadioButton = new System.Windows.Forms.RadioButton();
            this.GuiTimer = new System.Windows.Forms.Timer(this.components);
            this.MainTableLayoutPanel.SuspendLayout();
            this.MainStatusStrip.SuspendLayout();
            this.MainToolStrip.SuspendLayout();
            this.NmeaSentenceTabControl.SuspendLayout();
            this.GgaPage.SuspendLayout();
            this.GgaTableLayoutPanel.SuspendLayout();
            this.GllPage.SuspendLayout();
            this.GllTableLayoutPanel.SuspendLayout();
            this.GsaPage.SuspendLayout();
            this.GsaTableLayoutPanel.SuspendLayout();
            this.GsvPage.SuspendLayout();
            this.GsvTableLayoutPanel.SuspendLayout();
            this.GsvDataTableLayoutPanel.SuspendLayout();
            this.GsvSatellitesTableLayoutPanel.SuspendLayout();
            this.RmcPage.SuspendLayout();
            this.RmcTableLayoutPanel.SuspendLayout();
            this.VtgPage.SuspendLayout();
            this.VtgTableLayoutPanel.SuspendLayout();
            this.GeneralStatusGroupBox.SuspendLayout();
            this.CounterGroupBox.SuspendLayout();
            this.CountersTableLayoutPanel.SuspendLayout();
            this.GpsPositionGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // MySerialPort
            // 
            this.MySerialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.MySerialPort_DataReceived);
            // 
            // MainTableLayoutPanel
            // 
            this.MainTableLayoutPanel.ColumnCount = 2;
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MainTableLayoutPanel.Controls.Add(this.MainStatusStrip, 0, 2);
            this.MainTableLayoutPanel.Controls.Add(this.MainToolStrip, 0, 0);
            this.MainTableLayoutPanel.Controls.Add(this.NmeaSentenceTabControl, 1, 1);
            this.MainTableLayoutPanel.Controls.Add(this.GeneralStatusGroupBox, 0, 1);
            this.MainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.MainTableLayoutPanel.Name = "MainTableLayoutPanel";
            this.MainTableLayoutPanel.RowCount = 3;
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.MainTableLayoutPanel.Size = new System.Drawing.Size(584, 361);
            this.MainTableLayoutPanel.TabIndex = 2;
            // 
            // MainStatusStrip
            // 
            this.MainTableLayoutPanel.SetColumnSpan(this.MainStatusStrip, 2);
            this.MainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SerialPortStatusLabel});
            this.MainStatusStrip.Location = new System.Drawing.Point(0, 339);
            this.MainStatusStrip.Name = "MainStatusStrip";
            this.MainStatusStrip.Size = new System.Drawing.Size(584, 22);
            this.MainStatusStrip.TabIndex = 5;
            this.MainStatusStrip.Text = "statusStrip1";
            // 
            // SerialPortStatusLabel
            // 
            this.SerialPortStatusLabel.Name = "SerialPortStatusLabel";
            this.SerialPortStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // MainToolStrip
            // 
            this.MainTableLayoutPanel.SetColumnSpan(this.MainToolStrip, 2);
            this.MainToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.MainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SerialPortSelectButton,
            this.SerialMonitorButton});
            this.MainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Size = new System.Drawing.Size(584, 25);
            this.MainToolStrip.TabIndex = 2;
            this.MainToolStrip.Text = "Main Tool Strip";
            // 
            // SerialPortSelectButton
            // 
            this.SerialPortSelectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SerialPortSelectButton.Image = ((System.Drawing.Image)(resources.GetObject("SerialPortSelectButton.Image")));
            this.SerialPortSelectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SerialPortSelectButton.Name = "SerialPortSelectButton";
            this.SerialPortSelectButton.Size = new System.Drawing.Size(29, 22);
            this.SerialPortSelectButton.ToolTipText = "Serial Port";
            this.SerialPortSelectButton.DropDownOpening += new System.EventHandler(this.SerialPortSelectButton_DropDownOpening);
            this.SerialPortSelectButton.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SerialPortSelectButton_DropDownItemClicked);
            // 
            // SerialMonitorButton
            // 
            this.SerialMonitorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SerialMonitorButton.Image = ((System.Drawing.Image)(resources.GetObject("SerialMonitorButton.Image")));
            this.SerialMonitorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SerialMonitorButton.Name = "SerialMonitorButton";
            this.SerialMonitorButton.Size = new System.Drawing.Size(23, 22);
            this.SerialMonitorButton.Text = "Serial Monitor";
            this.SerialMonitorButton.ToolTipText = "Serial Monitor";
            this.SerialMonitorButton.Click += new System.EventHandler(this.SerialMonitorButton_Click);
            // 
            // NmeaSentenceTabControl
            // 
            this.NmeaSentenceTabControl.Controls.Add(this.GgaPage);
            this.NmeaSentenceTabControl.Controls.Add(this.GllPage);
            this.NmeaSentenceTabControl.Controls.Add(this.GsaPage);
            this.NmeaSentenceTabControl.Controls.Add(this.GsvPage);
            this.NmeaSentenceTabControl.Controls.Add(this.RmcPage);
            this.NmeaSentenceTabControl.Controls.Add(this.VtgPage);
            this.NmeaSentenceTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NmeaSentenceTabControl.Location = new System.Drawing.Point(295, 28);
            this.NmeaSentenceTabControl.Name = "NmeaSentenceTabControl";
            this.NmeaSentenceTabControl.SelectedIndex = 0;
            this.NmeaSentenceTabControl.Size = new System.Drawing.Size(286, 305);
            this.NmeaSentenceTabControl.TabIndex = 3;
            // 
            // GgaPage
            // 
            this.GgaPage.Controls.Add(this.GgaTableLayoutPanel);
            this.GgaPage.Location = new System.Drawing.Point(4, 22);
            this.GgaPage.Name = "GgaPage";
            this.GgaPage.Padding = new System.Windows.Forms.Padding(3);
            this.GgaPage.Size = new System.Drawing.Size(278, 279);
            this.GgaPage.TabIndex = 0;
            this.GgaPage.Text = "GGA";
            this.GgaPage.UseVisualStyleBackColor = true;
            // 
            // GgaTableLayoutPanel
            // 
            this.GgaTableLayoutPanel.ColumnCount = 2;
            this.GgaTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.GgaTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.GgaTableLayoutPanel.Controls.Add(this.UtcTimeTextBox, 1, 0);
            this.GgaTableLayoutPanel.Controls.Add(this.GgaPositionLabel, 0, 1);
            this.GgaTableLayoutPanel.Controls.Add(this.PositionTextBox, 1, 1);
            this.GgaTableLayoutPanel.Controls.Add(this.GgaGpsQualityLabel, 0, 2);
            this.GgaTableLayoutPanel.Controls.Add(this.GgaSatellitesUsedLabel, 0, 3);
            this.GgaTableLayoutPanel.Controls.Add(this.GgaHdopLabel, 0, 4);
            this.GgaTableLayoutPanel.Controls.Add(this.GgaAltitudeLabel, 0, 5);
            this.GgaTableLayoutPanel.Controls.Add(this.GpsQualityTextBox, 1, 2);
            this.GgaTableLayoutPanel.Controls.Add(this.SatellitesUsedTextBox, 1, 3);
            this.GgaTableLayoutPanel.Controls.Add(this.HorizontalDopTextBox, 1, 4);
            this.GgaTableLayoutPanel.Controls.Add(this.AltitudeTextBox, 1, 5);
            this.GgaTableLayoutPanel.Controls.Add(this.ChecksumTextBox, 1, 6);
            this.GgaTableLayoutPanel.Controls.Add(this.GgaChecksumLabel, 0, 6);
            this.GgaTableLayoutPanel.Controls.Add(this.GgaUtcTimeLabel, 0, 0);
            this.GgaTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GgaTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.GgaTableLayoutPanel.Name = "GgaTableLayoutPanel";
            this.GgaTableLayoutPanel.RowCount = 8;
            this.GgaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GgaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GgaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GgaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GgaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GgaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GgaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GgaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GgaTableLayoutPanel.Size = new System.Drawing.Size(272, 273);
            this.GgaTableLayoutPanel.TabIndex = 0;
            // 
            // UtcTimeTextBox
            // 
            this.UtcTimeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UtcTimeTextBox.Location = new System.Drawing.Point(93, 3);
            this.UtcTimeTextBox.Name = "UtcTimeTextBox";
            this.UtcTimeTextBox.ReadOnly = true;
            this.UtcTimeTextBox.Size = new System.Drawing.Size(176, 20);
            this.UtcTimeTextBox.TabIndex = 1;
            // 
            // GgaPositionLabel
            // 
            this.GgaPositionLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GgaPositionLabel.AutoSize = true;
            this.GgaPositionLabel.Location = new System.Drawing.Point(3, 31);
            this.GgaPositionLabel.Name = "GgaPositionLabel";
            this.GgaPositionLabel.Size = new System.Drawing.Size(44, 13);
            this.GgaPositionLabel.TabIndex = 2;
            this.GgaPositionLabel.Text = "Position";
            // 
            // PositionTextBox
            // 
            this.PositionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PositionTextBox.Location = new System.Drawing.Point(93, 28);
            this.PositionTextBox.Name = "PositionTextBox";
            this.PositionTextBox.ReadOnly = true;
            this.PositionTextBox.Size = new System.Drawing.Size(176, 20);
            this.PositionTextBox.TabIndex = 3;
            // 
            // GgaGpsQualityLabel
            // 
            this.GgaGpsQualityLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GgaGpsQualityLabel.AutoSize = true;
            this.GgaGpsQualityLabel.Location = new System.Drawing.Point(3, 56);
            this.GgaGpsQualityLabel.Name = "GgaGpsQualityLabel";
            this.GgaGpsQualityLabel.Size = new System.Drawing.Size(64, 13);
            this.GgaGpsQualityLabel.TabIndex = 4;
            this.GgaGpsQualityLabel.Text = "GPS Quality";
            // 
            // GgaSatellitesUsedLabel
            // 
            this.GgaSatellitesUsedLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GgaSatellitesUsedLabel.AutoSize = true;
            this.GgaSatellitesUsedLabel.Location = new System.Drawing.Point(3, 81);
            this.GgaSatellitesUsedLabel.Name = "GgaSatellitesUsedLabel";
            this.GgaSatellitesUsedLabel.Size = new System.Drawing.Size(77, 13);
            this.GgaSatellitesUsedLabel.TabIndex = 5;
            this.GgaSatellitesUsedLabel.Text = "Satellites Used";
            // 
            // GgaHdopLabel
            // 
            this.GgaHdopLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GgaHdopLabel.AutoSize = true;
            this.GgaHdopLabel.Location = new System.Drawing.Point(3, 106);
            this.GgaHdopLabel.Name = "GgaHdopLabel";
            this.GgaHdopLabel.Size = new System.Drawing.Size(80, 13);
            this.GgaHdopLabel.TabIndex = 6;
            this.GgaHdopLabel.Text = "Horizontal DOP";
            // 
            // GgaAltitudeLabel
            // 
            this.GgaAltitudeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GgaAltitudeLabel.AutoSize = true;
            this.GgaAltitudeLabel.Location = new System.Drawing.Point(3, 131);
            this.GgaAltitudeLabel.Name = "GgaAltitudeLabel";
            this.GgaAltitudeLabel.Size = new System.Drawing.Size(42, 13);
            this.GgaAltitudeLabel.TabIndex = 7;
            this.GgaAltitudeLabel.Text = "Altitude";
            // 
            // GpsQualityTextBox
            // 
            this.GpsQualityTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GpsQualityTextBox.Location = new System.Drawing.Point(93, 53);
            this.GpsQualityTextBox.Name = "GpsQualityTextBox";
            this.GpsQualityTextBox.ReadOnly = true;
            this.GpsQualityTextBox.Size = new System.Drawing.Size(176, 20);
            this.GpsQualityTextBox.TabIndex = 8;
            // 
            // SatellitesUsedTextBox
            // 
            this.SatellitesUsedTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SatellitesUsedTextBox.Location = new System.Drawing.Point(93, 78);
            this.SatellitesUsedTextBox.Name = "SatellitesUsedTextBox";
            this.SatellitesUsedTextBox.ReadOnly = true;
            this.SatellitesUsedTextBox.Size = new System.Drawing.Size(176, 20);
            this.SatellitesUsedTextBox.TabIndex = 9;
            // 
            // HorizontalDopTextBox
            // 
            this.HorizontalDopTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HorizontalDopTextBox.Location = new System.Drawing.Point(93, 103);
            this.HorizontalDopTextBox.Name = "HorizontalDopTextBox";
            this.HorizontalDopTextBox.ReadOnly = true;
            this.HorizontalDopTextBox.Size = new System.Drawing.Size(176, 20);
            this.HorizontalDopTextBox.TabIndex = 10;
            // 
            // AltitudeTextBox
            // 
            this.AltitudeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AltitudeTextBox.Location = new System.Drawing.Point(93, 128);
            this.AltitudeTextBox.Name = "AltitudeTextBox";
            this.AltitudeTextBox.ReadOnly = true;
            this.AltitudeTextBox.Size = new System.Drawing.Size(176, 20);
            this.AltitudeTextBox.TabIndex = 11;
            // 
            // ChecksumTextBox
            // 
            this.ChecksumTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChecksumTextBox.Location = new System.Drawing.Point(93, 153);
            this.ChecksumTextBox.Name = "ChecksumTextBox";
            this.ChecksumTextBox.ReadOnly = true;
            this.ChecksumTextBox.Size = new System.Drawing.Size(176, 20);
            this.ChecksumTextBox.TabIndex = 12;
            // 
            // GgaChecksumLabel
            // 
            this.GgaChecksumLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GgaChecksumLabel.AutoSize = true;
            this.GgaChecksumLabel.Location = new System.Drawing.Point(3, 156);
            this.GgaChecksumLabel.Name = "GgaChecksumLabel";
            this.GgaChecksumLabel.Size = new System.Drawing.Size(57, 13);
            this.GgaChecksumLabel.TabIndex = 13;
            this.GgaChecksumLabel.Text = "Checksum";
            // 
            // GgaUtcTimeLabel
            // 
            this.GgaUtcTimeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GgaUtcTimeLabel.AutoSize = true;
            this.GgaUtcTimeLabel.Location = new System.Drawing.Point(3, 6);
            this.GgaUtcTimeLabel.Name = "GgaUtcTimeLabel";
            this.GgaUtcTimeLabel.Size = new System.Drawing.Size(55, 13);
            this.GgaUtcTimeLabel.TabIndex = 0;
            this.GgaUtcTimeLabel.Text = "UTC Time";
            // 
            // GllPage
            // 
            this.GllPage.Controls.Add(this.GllTableLayoutPanel);
            this.GllPage.Location = new System.Drawing.Point(4, 22);
            this.GllPage.Name = "GllPage";
            this.GllPage.Padding = new System.Windows.Forms.Padding(3);
            this.GllPage.Size = new System.Drawing.Size(278, 279);
            this.GllPage.TabIndex = 1;
            this.GllPage.Text = "GLL";
            this.GllPage.UseVisualStyleBackColor = true;
            // 
            // GllTableLayoutPanel
            // 
            this.GllTableLayoutPanel.ColumnCount = 2;
            this.GllTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.GllTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.GllTableLayoutPanel.Controls.Add(this.GllStatusLabel, 0, 2);
            this.GllTableLayoutPanel.Controls.Add(this.GllModeIndicatorLabel, 0, 3);
            this.GllTableLayoutPanel.Controls.Add(this.GllChecksumLabel, 0, 4);
            this.GllTableLayoutPanel.Controls.Add(this.GllUtcTimeTextBox, 1, 0);
            this.GllTableLayoutPanel.Controls.Add(this.GllPositionTextBox, 1, 1);
            this.GllTableLayoutPanel.Controls.Add(this.GllStatusTextBox, 1, 2);
            this.GllTableLayoutPanel.Controls.Add(this.GllModeIndicatorTextBox, 1, 3);
            this.GllTableLayoutPanel.Controls.Add(this.GllChecksumTextBox, 1, 4);
            this.GllTableLayoutPanel.Controls.Add(this.GllUtcLabel, 0, 0);
            this.GllTableLayoutPanel.Controls.Add(this.PositionLabel, 0, 1);
            this.GllTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GllTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.GllTableLayoutPanel.Name = "GllTableLayoutPanel";
            this.GllTableLayoutPanel.RowCount = 6;
            this.GllTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GllTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GllTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GllTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GllTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GllTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GllTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GllTableLayoutPanel.Size = new System.Drawing.Size(272, 273);
            this.GllTableLayoutPanel.TabIndex = 0;
            // 
            // GllStatusLabel
            // 
            this.GllStatusLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GllStatusLabel.AutoSize = true;
            this.GllStatusLabel.Location = new System.Drawing.Point(3, 56);
            this.GllStatusLabel.Name = "GllStatusLabel";
            this.GllStatusLabel.Size = new System.Drawing.Size(37, 13);
            this.GllStatusLabel.TabIndex = 2;
            this.GllStatusLabel.Text = "Status";
            // 
            // GllModeIndicatorLabel
            // 
            this.GllModeIndicatorLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GllModeIndicatorLabel.AutoSize = true;
            this.GllModeIndicatorLabel.Location = new System.Drawing.Point(3, 81);
            this.GllModeIndicatorLabel.Name = "GllModeIndicatorLabel";
            this.GllModeIndicatorLabel.Size = new System.Drawing.Size(78, 13);
            this.GllModeIndicatorLabel.TabIndex = 3;
            this.GllModeIndicatorLabel.Text = "Mode Indicator";
            // 
            // GllChecksumLabel
            // 
            this.GllChecksumLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GllChecksumLabel.AutoSize = true;
            this.GllChecksumLabel.Location = new System.Drawing.Point(3, 106);
            this.GllChecksumLabel.Name = "GllChecksumLabel";
            this.GllChecksumLabel.Size = new System.Drawing.Size(57, 13);
            this.GllChecksumLabel.TabIndex = 4;
            this.GllChecksumLabel.Text = "Checksum";
            // 
            // GllUtcTimeTextBox
            // 
            this.GllUtcTimeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GllUtcTimeTextBox.Location = new System.Drawing.Point(93, 3);
            this.GllUtcTimeTextBox.Name = "GllUtcTimeTextBox";
            this.GllUtcTimeTextBox.ReadOnly = true;
            this.GllUtcTimeTextBox.Size = new System.Drawing.Size(176, 20);
            this.GllUtcTimeTextBox.TabIndex = 6;
            // 
            // GllPositionTextBox
            // 
            this.GllPositionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GllPositionTextBox.Location = new System.Drawing.Point(93, 28);
            this.GllPositionTextBox.Name = "GllPositionTextBox";
            this.GllPositionTextBox.ReadOnly = true;
            this.GllPositionTextBox.Size = new System.Drawing.Size(176, 20);
            this.GllPositionTextBox.TabIndex = 7;
            // 
            // GllStatusTextBox
            // 
            this.GllStatusTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GllStatusTextBox.Location = new System.Drawing.Point(93, 53);
            this.GllStatusTextBox.Name = "GllStatusTextBox";
            this.GllStatusTextBox.ReadOnly = true;
            this.GllStatusTextBox.Size = new System.Drawing.Size(176, 20);
            this.GllStatusTextBox.TabIndex = 8;
            // 
            // GllModeIndicatorTextBox
            // 
            this.GllModeIndicatorTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GllModeIndicatorTextBox.Location = new System.Drawing.Point(93, 78);
            this.GllModeIndicatorTextBox.Name = "GllModeIndicatorTextBox";
            this.GllModeIndicatorTextBox.ReadOnly = true;
            this.GllModeIndicatorTextBox.Size = new System.Drawing.Size(176, 20);
            this.GllModeIndicatorTextBox.TabIndex = 9;
            // 
            // GllChecksumTextBox
            // 
            this.GllChecksumTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GllChecksumTextBox.Location = new System.Drawing.Point(93, 103);
            this.GllChecksumTextBox.Name = "GllChecksumTextBox";
            this.GllChecksumTextBox.ReadOnly = true;
            this.GllChecksumTextBox.Size = new System.Drawing.Size(176, 20);
            this.GllChecksumTextBox.TabIndex = 10;
            // 
            // GllUtcLabel
            // 
            this.GllUtcLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GllUtcLabel.AutoSize = true;
            this.GllUtcLabel.Location = new System.Drawing.Point(3, 6);
            this.GllUtcLabel.Name = "GllUtcLabel";
            this.GllUtcLabel.Size = new System.Drawing.Size(55, 13);
            this.GllUtcLabel.TabIndex = 1;
            this.GllUtcLabel.Text = "UTC Time";
            // 
            // PositionLabel
            // 
            this.PositionLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.PositionLabel.AutoSize = true;
            this.PositionLabel.Location = new System.Drawing.Point(3, 31);
            this.PositionLabel.Name = "PositionLabel";
            this.PositionLabel.Size = new System.Drawing.Size(44, 13);
            this.PositionLabel.TabIndex = 0;
            this.PositionLabel.Text = "Position";
            // 
            // GsaPage
            // 
            this.GsaPage.Controls.Add(this.GsaTableLayoutPanel);
            this.GsaPage.Location = new System.Drawing.Point(4, 22);
            this.GsaPage.Name = "GsaPage";
            this.GsaPage.Padding = new System.Windows.Forms.Padding(3);
            this.GsaPage.Size = new System.Drawing.Size(278, 279);
            this.GsaPage.TabIndex = 2;
            this.GsaPage.Text = "GSA";
            this.GsaPage.UseVisualStyleBackColor = true;
            // 
            // GsaTableLayoutPanel
            // 
            this.GsaTableLayoutPanel.ColumnCount = 2;
            this.GsaTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.GsaTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.GsaTableLayoutPanel.Controls.Add(this.GsaFixModeLabel, 0, 0);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaFixTypeLabel, 0, 1);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaSatellitesUsedLabel, 0, 2);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaPositionDopLabel, 0, 3);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaHorizontalDopLabel, 0, 4);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaVerticalDopLabel, 0, 5);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaFixModeTextBox, 1, 0);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaFixTypeTextBox, 1, 1);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaSatellitesUsedTextBox, 1, 2);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaPositionDopTextBox, 1, 3);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaHorizontalDopTextBox, 1, 4);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaVerticalDopTextBox, 1, 5);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaChecksumTextBox, 1, 6);
            this.GsaTableLayoutPanel.Controls.Add(this.GsaChecksumLabel, 0, 6);
            this.GsaTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsaTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.GsaTableLayoutPanel.Name = "GsaTableLayoutPanel";
            this.GsaTableLayoutPanel.RowCount = 8;
            this.GsaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GsaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GsaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GsaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GsaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GsaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GsaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.GsaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsaTableLayoutPanel.Size = new System.Drawing.Size(272, 273);
            this.GsaTableLayoutPanel.TabIndex = 0;
            // 
            // GsaFixModeLabel
            // 
            this.GsaFixModeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsaFixModeLabel.AutoSize = true;
            this.GsaFixModeLabel.Location = new System.Drawing.Point(3, 6);
            this.GsaFixModeLabel.Name = "GsaFixModeLabel";
            this.GsaFixModeLabel.Size = new System.Drawing.Size(50, 13);
            this.GsaFixModeLabel.TabIndex = 0;
            this.GsaFixModeLabel.Text = "Fix Mode";
            // 
            // GsaFixTypeLabel
            // 
            this.GsaFixTypeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsaFixTypeLabel.AutoSize = true;
            this.GsaFixTypeLabel.Location = new System.Drawing.Point(3, 31);
            this.GsaFixTypeLabel.Name = "GsaFixTypeLabel";
            this.GsaFixTypeLabel.Size = new System.Drawing.Size(47, 13);
            this.GsaFixTypeLabel.TabIndex = 1;
            this.GsaFixTypeLabel.Text = "Fix Type";
            // 
            // GsaSatellitesUsedLabel
            // 
            this.GsaSatellitesUsedLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsaSatellitesUsedLabel.AutoSize = true;
            this.GsaSatellitesUsedLabel.Location = new System.Drawing.Point(3, 56);
            this.GsaSatellitesUsedLabel.Name = "GsaSatellitesUsedLabel";
            this.GsaSatellitesUsedLabel.Size = new System.Drawing.Size(77, 13);
            this.GsaSatellitesUsedLabel.TabIndex = 2;
            this.GsaSatellitesUsedLabel.Text = "Satellites Used";
            // 
            // GsaPositionDopLabel
            // 
            this.GsaPositionDopLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsaPositionDopLabel.AutoSize = true;
            this.GsaPositionDopLabel.Location = new System.Drawing.Point(3, 81);
            this.GsaPositionDopLabel.Name = "GsaPositionDopLabel";
            this.GsaPositionDopLabel.Size = new System.Drawing.Size(70, 13);
            this.GsaPositionDopLabel.TabIndex = 3;
            this.GsaPositionDopLabel.Text = "Position DOP";
            // 
            // GsaHorizontalDopLabel
            // 
            this.GsaHorizontalDopLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsaHorizontalDopLabel.AutoSize = true;
            this.GsaHorizontalDopLabel.Location = new System.Drawing.Point(3, 106);
            this.GsaHorizontalDopLabel.Name = "GsaHorizontalDopLabel";
            this.GsaHorizontalDopLabel.Size = new System.Drawing.Size(80, 13);
            this.GsaHorizontalDopLabel.TabIndex = 4;
            this.GsaHorizontalDopLabel.Text = "Horizontal DOP";
            // 
            // GsaVerticalDopLabel
            // 
            this.GsaVerticalDopLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsaVerticalDopLabel.AutoSize = true;
            this.GsaVerticalDopLabel.Location = new System.Drawing.Point(3, 131);
            this.GsaVerticalDopLabel.Name = "GsaVerticalDopLabel";
            this.GsaVerticalDopLabel.Size = new System.Drawing.Size(68, 13);
            this.GsaVerticalDopLabel.TabIndex = 5;
            this.GsaVerticalDopLabel.Text = "Vertical DOP";
            // 
            // GsaFixModeTextBox
            // 
            this.GsaFixModeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsaFixModeTextBox.Location = new System.Drawing.Point(93, 3);
            this.GsaFixModeTextBox.Name = "GsaFixModeTextBox";
            this.GsaFixModeTextBox.ReadOnly = true;
            this.GsaFixModeTextBox.Size = new System.Drawing.Size(176, 20);
            this.GsaFixModeTextBox.TabIndex = 7;
            // 
            // GsaFixTypeTextBox
            // 
            this.GsaFixTypeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsaFixTypeTextBox.Location = new System.Drawing.Point(93, 28);
            this.GsaFixTypeTextBox.Name = "GsaFixTypeTextBox";
            this.GsaFixTypeTextBox.ReadOnly = true;
            this.GsaFixTypeTextBox.Size = new System.Drawing.Size(176, 20);
            this.GsaFixTypeTextBox.TabIndex = 8;
            // 
            // GsaSatellitesUsedTextBox
            // 
            this.GsaSatellitesUsedTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsaSatellitesUsedTextBox.Location = new System.Drawing.Point(93, 53);
            this.GsaSatellitesUsedTextBox.Name = "GsaSatellitesUsedTextBox";
            this.GsaSatellitesUsedTextBox.ReadOnly = true;
            this.GsaSatellitesUsedTextBox.Size = new System.Drawing.Size(176, 20);
            this.GsaSatellitesUsedTextBox.TabIndex = 9;
            // 
            // GsaPositionDopTextBox
            // 
            this.GsaPositionDopTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsaPositionDopTextBox.Location = new System.Drawing.Point(93, 78);
            this.GsaPositionDopTextBox.Name = "GsaPositionDopTextBox";
            this.GsaPositionDopTextBox.ReadOnly = true;
            this.GsaPositionDopTextBox.Size = new System.Drawing.Size(176, 20);
            this.GsaPositionDopTextBox.TabIndex = 10;
            // 
            // GsaHorizontalDopTextBox
            // 
            this.GsaHorizontalDopTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsaHorizontalDopTextBox.Location = new System.Drawing.Point(93, 103);
            this.GsaHorizontalDopTextBox.Name = "GsaHorizontalDopTextBox";
            this.GsaHorizontalDopTextBox.ReadOnly = true;
            this.GsaHorizontalDopTextBox.Size = new System.Drawing.Size(176, 20);
            this.GsaHorizontalDopTextBox.TabIndex = 11;
            // 
            // GsaVerticalDopTextBox
            // 
            this.GsaVerticalDopTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsaVerticalDopTextBox.Location = new System.Drawing.Point(93, 128);
            this.GsaVerticalDopTextBox.Name = "GsaVerticalDopTextBox";
            this.GsaVerticalDopTextBox.ReadOnly = true;
            this.GsaVerticalDopTextBox.Size = new System.Drawing.Size(176, 20);
            this.GsaVerticalDopTextBox.TabIndex = 12;
            // 
            // GsaChecksumTextBox
            // 
            this.GsaChecksumTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsaChecksumTextBox.Location = new System.Drawing.Point(93, 153);
            this.GsaChecksumTextBox.Name = "GsaChecksumTextBox";
            this.GsaChecksumTextBox.ReadOnly = true;
            this.GsaChecksumTextBox.Size = new System.Drawing.Size(176, 20);
            this.GsaChecksumTextBox.TabIndex = 13;
            // 
            // GsaChecksumLabel
            // 
            this.GsaChecksumLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsaChecksumLabel.AutoSize = true;
            this.GsaChecksumLabel.Location = new System.Drawing.Point(3, 156);
            this.GsaChecksumLabel.Name = "GsaChecksumLabel";
            this.GsaChecksumLabel.Size = new System.Drawing.Size(57, 13);
            this.GsaChecksumLabel.TabIndex = 6;
            this.GsaChecksumLabel.Text = "Checksum";
            // 
            // GsvPage
            // 
            this.GsvPage.Controls.Add(this.GsvTableLayoutPanel);
            this.GsvPage.Location = new System.Drawing.Point(4, 22);
            this.GsvPage.Name = "GsvPage";
            this.GsvPage.Padding = new System.Windows.Forms.Padding(3);
            this.GsvPage.Size = new System.Drawing.Size(278, 279);
            this.GsvPage.TabIndex = 3;
            this.GsvPage.Text = "GSV";
            this.GsvPage.UseVisualStyleBackColor = true;
            // 
            // GsvTableLayoutPanel
            // 
            this.GsvTableLayoutPanel.ColumnCount = 2;
            this.GsvTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.GsvTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.GsvTableLayoutPanel.Controls.Add(this.GsvDataTableLayoutPanel, 0, 0);
            this.GsvTableLayoutPanel.Controls.Add(this.GsvSatellitesTableLayoutPanel, 0, 0);
            this.GsvTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsvTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.GsvTableLayoutPanel.Name = "GsvTableLayoutPanel";
            this.GsvTableLayoutPanel.RowCount = 1;
            this.GsvTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.GsvTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 273F));
            this.GsvTableLayoutPanel.Size = new System.Drawing.Size(272, 273);
            this.GsvTableLayoutPanel.TabIndex = 0;
            // 
            // GsvDataTableLayoutPanel
            // 
            this.GsvDataTableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.GsvDataTableLayoutPanel.ColumnCount = 1;
            this.GsvDataTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.GsvDataTableLayoutPanel.Controls.Add(this.GsvTotalMessagesLabel, 0, 0);
            this.GsvDataTableLayoutPanel.Controls.Add(this.GsvSatellitesInViewLabel, 0, 4);
            this.GsvDataTableLayoutPanel.Controls.Add(this.GsvSequenceNumberLabel, 0, 2);
            this.GsvDataTableLayoutPanel.Controls.Add(this.GsvMessageCountText, 0, 1);
            this.GsvDataTableLayoutPanel.Controls.Add(this.GsvSequenceNumberText, 0, 3);
            this.GsvDataTableLayoutPanel.Controls.Add(this.GsvSatellitesNumberText, 0, 5);
            this.GsvDataTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsvDataTableLayoutPanel.Location = new System.Drawing.Point(220, 3);
            this.GsvDataTableLayoutPanel.Name = "GsvDataTableLayoutPanel";
            this.GsvDataTableLayoutPanel.RowCount = 7;
            this.GsvDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.GsvDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsvDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.GsvDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsvDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.GsvDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsvDataTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsvDataTableLayoutPanel.Size = new System.Drawing.Size(49, 267);
            this.GsvDataTableLayoutPanel.TabIndex = 3;
            // 
            // GsvTotalMessagesLabel
            // 
            this.GsvTotalMessagesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GsvTotalMessagesLabel.AutoSize = true;
            this.GsvTotalMessagesLabel.Location = new System.Drawing.Point(4, 4);
            this.GsvTotalMessagesLabel.Name = "GsvTotalMessagesLabel";
            this.GsvTotalMessagesLabel.Size = new System.Drawing.Size(41, 13);
            this.GsvTotalMessagesLabel.TabIndex = 0;
            this.GsvTotalMessagesLabel.Text = "MSGs";
            this.GsvTotalMessagesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GsvSatellitesInViewLabel
            // 
            this.GsvSatellitesInViewLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GsvSatellitesInViewLabel.AutoSize = true;
            this.GsvSatellitesInViewLabel.Location = new System.Drawing.Point(4, 86);
            this.GsvSatellitesInViewLabel.Name = "GsvSatellitesInViewLabel";
            this.GsvSatellitesInViewLabel.Size = new System.Drawing.Size(41, 13);
            this.GsvSatellitesInViewLabel.TabIndex = 2;
            this.GsvSatellitesInViewLabel.Text = "SATs";
            this.GsvSatellitesInViewLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GsvSequenceNumberLabel
            // 
            this.GsvSequenceNumberLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GsvSequenceNumberLabel.AutoSize = true;
            this.GsvSequenceNumberLabel.Location = new System.Drawing.Point(4, 45);
            this.GsvSequenceNumberLabel.Name = "GsvSequenceNumberLabel";
            this.GsvSequenceNumberLabel.Size = new System.Drawing.Size(41, 13);
            this.GsvSequenceNumberLabel.TabIndex = 1;
            this.GsvSequenceNumberLabel.Text = "SEQ#";
            this.GsvSequenceNumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GsvMessageCountText
            // 
            this.GsvMessageCountText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GsvMessageCountText.AutoSize = true;
            this.GsvMessageCountText.Location = new System.Drawing.Point(4, 24);
            this.GsvMessageCountText.Name = "GsvMessageCountText";
            this.GsvMessageCountText.Size = new System.Drawing.Size(41, 13);
            this.GsvMessageCountText.TabIndex = 3;
            this.GsvMessageCountText.Text = "0";
            this.GsvMessageCountText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GsvSequenceNumberText
            // 
            this.GsvSequenceNumberText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GsvSequenceNumberText.AutoSize = true;
            this.GsvSequenceNumberText.Location = new System.Drawing.Point(4, 65);
            this.GsvSequenceNumberText.Name = "GsvSequenceNumberText";
            this.GsvSequenceNumberText.Size = new System.Drawing.Size(41, 13);
            this.GsvSequenceNumberText.TabIndex = 4;
            this.GsvSequenceNumberText.Text = "0";
            this.GsvSequenceNumberText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GsvSatellitesNumberText
            // 
            this.GsvSatellitesNumberText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GsvSatellitesNumberText.AutoSize = true;
            this.GsvSatellitesNumberText.Location = new System.Drawing.Point(4, 106);
            this.GsvSatellitesNumberText.Name = "GsvSatellitesNumberText";
            this.GsvSatellitesNumberText.Size = new System.Drawing.Size(41, 13);
            this.GsvSatellitesNumberText.TabIndex = 5;
            this.GsvSatellitesNumberText.Text = "0";
            this.GsvSatellitesNumberText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GsvSatellitesTableLayoutPanel
            // 
            this.GsvSatellitesTableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.GsvSatellitesTableLayoutPanel.ColumnCount = 5;
            this.GsvSatellitesTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.GsvSatellitesTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.GsvSatellitesTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.GsvSatellitesTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.GsvSatellitesTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel1, 0, 1);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel2, 0, 2);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel3, 0, 3);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel4, 0, 4);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel5, 0, 5);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel6, 0, 6);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel7, 0, 7);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel8, 0, 8);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel9, 0, 9);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel10, 0, 10);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel11, 0, 11);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteLabel12, 0, 12);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvIdLabel, 1, 0);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvElevationLabel, 2, 0);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvAzimuthLabel, 3, 0);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSnrLabel, 4, 0);
            this.GsvSatellitesTableLayoutPanel.Controls.Add(this.GsvSatelliteIndex, 0, 0);
            this.GsvSatellitesTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsvSatellitesTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.GsvSatellitesTableLayoutPanel.Name = "GsvSatellitesTableLayoutPanel";
            this.GsvSatellitesTableLayoutPanel.RowCount = 13;
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GsvSatellitesTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.GsvSatellitesTableLayoutPanel.Size = new System.Drawing.Size(211, 267);
            this.GsvSatellitesTableLayoutPanel.TabIndex = 1;
            // 
            // GsvSatelliteLabel1
            // 
            this.GsvSatelliteLabel1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel1.AutoSize = true;
            this.GsvSatelliteLabel1.Location = new System.Drawing.Point(4, 24);
            this.GsvSatelliteLabel1.Name = "GsvSatelliteLabel1";
            this.GsvSatelliteLabel1.Size = new System.Drawing.Size(13, 13);
            this.GsvSatelliteLabel1.TabIndex = 0;
            this.GsvSatelliteLabel1.Text = "1";
            // 
            // GsvSatelliteLabel2
            // 
            this.GsvSatelliteLabel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel2.AutoSize = true;
            this.GsvSatelliteLabel2.Location = new System.Drawing.Point(4, 45);
            this.GsvSatelliteLabel2.Name = "GsvSatelliteLabel2";
            this.GsvSatelliteLabel2.Size = new System.Drawing.Size(13, 13);
            this.GsvSatelliteLabel2.TabIndex = 1;
            this.GsvSatelliteLabel2.Text = "2";
            // 
            // GsvSatelliteLabel3
            // 
            this.GsvSatelliteLabel3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel3.AutoSize = true;
            this.GsvSatelliteLabel3.Location = new System.Drawing.Point(4, 65);
            this.GsvSatelliteLabel3.Name = "GsvSatelliteLabel3";
            this.GsvSatelliteLabel3.Size = new System.Drawing.Size(13, 13);
            this.GsvSatelliteLabel3.TabIndex = 2;
            this.GsvSatelliteLabel3.Text = "3";
            // 
            // GsvSatelliteLabel4
            // 
            this.GsvSatelliteLabel4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel4.AutoSize = true;
            this.GsvSatelliteLabel4.Location = new System.Drawing.Point(4, 86);
            this.GsvSatelliteLabel4.Name = "GsvSatelliteLabel4";
            this.GsvSatelliteLabel4.Size = new System.Drawing.Size(13, 13);
            this.GsvSatelliteLabel4.TabIndex = 3;
            this.GsvSatelliteLabel4.Text = "4";
            // 
            // GsvSatelliteLabel5
            // 
            this.GsvSatelliteLabel5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel5.AutoSize = true;
            this.GsvSatelliteLabel5.Location = new System.Drawing.Point(4, 106);
            this.GsvSatelliteLabel5.Name = "GsvSatelliteLabel5";
            this.GsvSatelliteLabel5.Size = new System.Drawing.Size(13, 13);
            this.GsvSatelliteLabel5.TabIndex = 4;
            this.GsvSatelliteLabel5.Text = "5";
            // 
            // GsvSatelliteLabel6
            // 
            this.GsvSatelliteLabel6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel6.AutoSize = true;
            this.GsvSatelliteLabel6.Location = new System.Drawing.Point(4, 127);
            this.GsvSatelliteLabel6.Name = "GsvSatelliteLabel6";
            this.GsvSatelliteLabel6.Size = new System.Drawing.Size(13, 13);
            this.GsvSatelliteLabel6.TabIndex = 5;
            this.GsvSatelliteLabel6.Text = "6";
            // 
            // GsvSatelliteLabel7
            // 
            this.GsvSatelliteLabel7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel7.AutoSize = true;
            this.GsvSatelliteLabel7.Location = new System.Drawing.Point(4, 147);
            this.GsvSatelliteLabel7.Name = "GsvSatelliteLabel7";
            this.GsvSatelliteLabel7.Size = new System.Drawing.Size(13, 13);
            this.GsvSatelliteLabel7.TabIndex = 6;
            this.GsvSatelliteLabel7.Text = "7";
            // 
            // GsvSatelliteLabel8
            // 
            this.GsvSatelliteLabel8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel8.AutoSize = true;
            this.GsvSatelliteLabel8.Location = new System.Drawing.Point(4, 168);
            this.GsvSatelliteLabel8.Name = "GsvSatelliteLabel8";
            this.GsvSatelliteLabel8.Size = new System.Drawing.Size(13, 13);
            this.GsvSatelliteLabel8.TabIndex = 7;
            this.GsvSatelliteLabel8.Text = "8";
            // 
            // GsvSatelliteLabel9
            // 
            this.GsvSatelliteLabel9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel9.AutoSize = true;
            this.GsvSatelliteLabel9.Location = new System.Drawing.Point(4, 188);
            this.GsvSatelliteLabel9.Name = "GsvSatelliteLabel9";
            this.GsvSatelliteLabel9.Size = new System.Drawing.Size(13, 13);
            this.GsvSatelliteLabel9.TabIndex = 8;
            this.GsvSatelliteLabel9.Text = "9";
            // 
            // GsvSatelliteLabel10
            // 
            this.GsvSatelliteLabel10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel10.AutoSize = true;
            this.GsvSatelliteLabel10.Location = new System.Drawing.Point(4, 209);
            this.GsvSatelliteLabel10.Name = "GsvSatelliteLabel10";
            this.GsvSatelliteLabel10.Size = new System.Drawing.Size(19, 13);
            this.GsvSatelliteLabel10.TabIndex = 9;
            this.GsvSatelliteLabel10.Text = "10";
            // 
            // GsvSatelliteLabel11
            // 
            this.GsvSatelliteLabel11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel11.AutoSize = true;
            this.GsvSatelliteLabel11.Location = new System.Drawing.Point(4, 229);
            this.GsvSatelliteLabel11.Name = "GsvSatelliteLabel11";
            this.GsvSatelliteLabel11.Size = new System.Drawing.Size(19, 13);
            this.GsvSatelliteLabel11.TabIndex = 10;
            this.GsvSatelliteLabel11.Text = "11";
            // 
            // GsvSatelliteLabel12
            // 
            this.GsvSatelliteLabel12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteLabel12.AutoSize = true;
            this.GsvSatelliteLabel12.Location = new System.Drawing.Point(4, 250);
            this.GsvSatelliteLabel12.Name = "GsvSatelliteLabel12";
            this.GsvSatelliteLabel12.Size = new System.Drawing.Size(19, 13);
            this.GsvSatelliteLabel12.TabIndex = 11;
            this.GsvSatelliteLabel12.Text = "12";
            // 
            // GsvIdLabel
            // 
            this.GsvIdLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GsvIdLabel.AutoSize = true;
            this.GsvIdLabel.Location = new System.Drawing.Point(46, 4);
            this.GsvIdLabel.Name = "GsvIdLabel";
            this.GsvIdLabel.Size = new System.Drawing.Size(35, 13);
            this.GsvIdLabel.TabIndex = 12;
            this.GsvIdLabel.Text = "ID";
            this.GsvIdLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GsvElevationLabel
            // 
            this.GsvElevationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GsvElevationLabel.AutoSize = true;
            this.GsvElevationLabel.Location = new System.Drawing.Point(88, 4);
            this.GsvElevationLabel.Name = "GsvElevationLabel";
            this.GsvElevationLabel.Size = new System.Drawing.Size(35, 13);
            this.GsvElevationLabel.TabIndex = 13;
            this.GsvElevationLabel.Text = "Elev";
            this.GsvElevationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GsvAzimuthLabel
            // 
            this.GsvAzimuthLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GsvAzimuthLabel.AutoSize = true;
            this.GsvAzimuthLabel.Location = new System.Drawing.Point(130, 4);
            this.GsvAzimuthLabel.Name = "GsvAzimuthLabel";
            this.GsvAzimuthLabel.Size = new System.Drawing.Size(35, 13);
            this.GsvAzimuthLabel.TabIndex = 14;
            this.GsvAzimuthLabel.Text = "Azim";
            this.GsvAzimuthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GsvSnrLabel
            // 
            this.GsvSnrLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GsvSnrLabel.AutoSize = true;
            this.GsvSnrLabel.Location = new System.Drawing.Point(172, 4);
            this.GsvSnrLabel.Name = "GsvSnrLabel";
            this.GsvSnrLabel.Size = new System.Drawing.Size(35, 13);
            this.GsvSnrLabel.TabIndex = 15;
            this.GsvSnrLabel.Text = "SNR";
            this.GsvSnrLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GsvSatelliteIndex
            // 
            this.GsvSatelliteIndex.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvSatelliteIndex.AutoSize = true;
            this.GsvSatelliteIndex.Location = new System.Drawing.Point(4, 4);
            this.GsvSatelliteIndex.Name = "GsvSatelliteIndex";
            this.GsvSatelliteIndex.Size = new System.Drawing.Size(33, 13);
            this.GsvSatelliteIndex.TabIndex = 16;
            this.GsvSatelliteIndex.Text = "Index";
            // 
            // RmcPage
            // 
            this.RmcPage.Controls.Add(this.RmcTableLayoutPanel);
            this.RmcPage.Location = new System.Drawing.Point(4, 22);
            this.RmcPage.Name = "RmcPage";
            this.RmcPage.Padding = new System.Windows.Forms.Padding(3);
            this.RmcPage.Size = new System.Drawing.Size(278, 279);
            this.RmcPage.TabIndex = 4;
            this.RmcPage.Text = "RMC";
            this.RmcPage.UseVisualStyleBackColor = true;
            // 
            // RmcTableLayoutPanel
            // 
            this.RmcTableLayoutPanel.ColumnCount = 2;
            this.RmcTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.RmcTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.RmcTableLayoutPanel.Controls.Add(this.RmcUtcTimeLabel, 0, 0);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcChecksumLabel, 0, 6);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcUtcDateTimeTextBox, 1, 0);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcPositionTextBox, 1, 1);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcStatusTextBox, 1, 2);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcModeIndicatorTextBox, 1, 3);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcGroundSpeedTextBox, 1, 4);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcTrackAngleTextBox, 1, 5);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcChecksumTextBox, 1, 6);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcStatusLabel, 0, 2);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcPositionLabel, 0, 1);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcGroundSpeedLabel, 0, 4);
            this.RmcTableLayoutPanel.Controls.Add(this.RmcModeIndicatorLabel, 0, 3);
            this.RmcTableLayoutPanel.Controls.Add(this.RcmTrackAngleLabel, 0, 5);
            this.RmcTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RmcTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.RmcTableLayoutPanel.Name = "RmcTableLayoutPanel";
            this.RmcTableLayoutPanel.RowCount = 8;
            this.RmcTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.RmcTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.RmcTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.RmcTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.RmcTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.RmcTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.RmcTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.RmcTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.RmcTableLayoutPanel.Size = new System.Drawing.Size(272, 273);
            this.RmcTableLayoutPanel.TabIndex = 0;
            // 
            // RmcUtcTimeLabel
            // 
            this.RmcUtcTimeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RmcUtcTimeLabel.AutoSize = true;
            this.RmcUtcTimeLabel.Location = new System.Drawing.Point(3, 6);
            this.RmcUtcTimeLabel.Name = "RmcUtcTimeLabel";
            this.RmcUtcTimeLabel.Size = new System.Drawing.Size(83, 13);
            this.RmcUtcTimeLabel.TabIndex = 0;
            this.RmcUtcTimeLabel.Text = "UTC Date/Time";
            // 
            // RmcChecksumLabel
            // 
            this.RmcChecksumLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RmcChecksumLabel.AutoSize = true;
            this.RmcChecksumLabel.Location = new System.Drawing.Point(3, 156);
            this.RmcChecksumLabel.Name = "RmcChecksumLabel";
            this.RmcChecksumLabel.Size = new System.Drawing.Size(51, 13);
            this.RmcChecksumLabel.TabIndex = 6;
            this.RmcChecksumLabel.Text = "Checsum";
            // 
            // RmcUtcDateTimeTextBox
            // 
            this.RmcUtcDateTimeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RmcUtcDateTimeTextBox.Location = new System.Drawing.Point(93, 3);
            this.RmcUtcDateTimeTextBox.Name = "RmcUtcDateTimeTextBox";
            this.RmcUtcDateTimeTextBox.ReadOnly = true;
            this.RmcUtcDateTimeTextBox.Size = new System.Drawing.Size(176, 20);
            this.RmcUtcDateTimeTextBox.TabIndex = 7;
            // 
            // RmcPositionTextBox
            // 
            this.RmcPositionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RmcPositionTextBox.Location = new System.Drawing.Point(93, 28);
            this.RmcPositionTextBox.Name = "RmcPositionTextBox";
            this.RmcPositionTextBox.ReadOnly = true;
            this.RmcPositionTextBox.Size = new System.Drawing.Size(176, 20);
            this.RmcPositionTextBox.TabIndex = 8;
            // 
            // RmcStatusTextBox
            // 
            this.RmcStatusTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RmcStatusTextBox.Location = new System.Drawing.Point(93, 53);
            this.RmcStatusTextBox.Name = "RmcStatusTextBox";
            this.RmcStatusTextBox.ReadOnly = true;
            this.RmcStatusTextBox.Size = new System.Drawing.Size(176, 20);
            this.RmcStatusTextBox.TabIndex = 9;
            // 
            // RmcModeIndicatorTextBox
            // 
            this.RmcModeIndicatorTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RmcModeIndicatorTextBox.Location = new System.Drawing.Point(93, 78);
            this.RmcModeIndicatorTextBox.Name = "RmcModeIndicatorTextBox";
            this.RmcModeIndicatorTextBox.ReadOnly = true;
            this.RmcModeIndicatorTextBox.Size = new System.Drawing.Size(176, 20);
            this.RmcModeIndicatorTextBox.TabIndex = 10;
            // 
            // RmcGroundSpeedTextBox
            // 
            this.RmcGroundSpeedTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RmcGroundSpeedTextBox.Location = new System.Drawing.Point(93, 103);
            this.RmcGroundSpeedTextBox.Name = "RmcGroundSpeedTextBox";
            this.RmcGroundSpeedTextBox.ReadOnly = true;
            this.RmcGroundSpeedTextBox.Size = new System.Drawing.Size(176, 20);
            this.RmcGroundSpeedTextBox.TabIndex = 11;
            // 
            // RmcTrackAngleTextBox
            // 
            this.RmcTrackAngleTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RmcTrackAngleTextBox.Location = new System.Drawing.Point(93, 128);
            this.RmcTrackAngleTextBox.Name = "RmcTrackAngleTextBox";
            this.RmcTrackAngleTextBox.ReadOnly = true;
            this.RmcTrackAngleTextBox.Size = new System.Drawing.Size(176, 20);
            this.RmcTrackAngleTextBox.TabIndex = 12;
            // 
            // RmcChecksumTextBox
            // 
            this.RmcChecksumTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RmcChecksumTextBox.Location = new System.Drawing.Point(93, 153);
            this.RmcChecksumTextBox.Name = "RmcChecksumTextBox";
            this.RmcChecksumTextBox.ReadOnly = true;
            this.RmcChecksumTextBox.Size = new System.Drawing.Size(176, 20);
            this.RmcChecksumTextBox.TabIndex = 13;
            // 
            // RmcStatusLabel
            // 
            this.RmcStatusLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RmcStatusLabel.AutoSize = true;
            this.RmcStatusLabel.Location = new System.Drawing.Point(3, 56);
            this.RmcStatusLabel.Name = "RmcStatusLabel";
            this.RmcStatusLabel.Size = new System.Drawing.Size(37, 13);
            this.RmcStatusLabel.TabIndex = 1;
            this.RmcStatusLabel.Text = "Status";
            // 
            // RmcPositionLabel
            // 
            this.RmcPositionLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RmcPositionLabel.AutoSize = true;
            this.RmcPositionLabel.Location = new System.Drawing.Point(3, 31);
            this.RmcPositionLabel.Name = "RmcPositionLabel";
            this.RmcPositionLabel.Size = new System.Drawing.Size(44, 13);
            this.RmcPositionLabel.TabIndex = 2;
            this.RmcPositionLabel.Text = "Position";
            // 
            // RmcGroundSpeedLabel
            // 
            this.RmcGroundSpeedLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RmcGroundSpeedLabel.AutoSize = true;
            this.RmcGroundSpeedLabel.Location = new System.Drawing.Point(3, 106);
            this.RmcGroundSpeedLabel.Name = "RmcGroundSpeedLabel";
            this.RmcGroundSpeedLabel.Size = new System.Drawing.Size(76, 13);
            this.RmcGroundSpeedLabel.TabIndex = 3;
            this.RmcGroundSpeedLabel.Text = "Ground Speed";
            // 
            // RmcModeIndicatorLabel
            // 
            this.RmcModeIndicatorLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RmcModeIndicatorLabel.AutoSize = true;
            this.RmcModeIndicatorLabel.Location = new System.Drawing.Point(3, 81);
            this.RmcModeIndicatorLabel.Name = "RmcModeIndicatorLabel";
            this.RmcModeIndicatorLabel.Size = new System.Drawing.Size(78, 13);
            this.RmcModeIndicatorLabel.TabIndex = 5;
            this.RmcModeIndicatorLabel.Text = "Mode Indicator";
            // 
            // RcmTrackAngleLabel
            // 
            this.RcmTrackAngleLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RcmTrackAngleLabel.AutoSize = true;
            this.RcmTrackAngleLabel.Location = new System.Drawing.Point(3, 131);
            this.RcmTrackAngleLabel.Name = "RcmTrackAngleLabel";
            this.RcmTrackAngleLabel.Size = new System.Drawing.Size(65, 13);
            this.RcmTrackAngleLabel.TabIndex = 4;
            this.RcmTrackAngleLabel.Text = "Track Angle";
            // 
            // VtgPage
            // 
            this.VtgPage.Controls.Add(this.VtgTableLayoutPanel);
            this.VtgPage.Location = new System.Drawing.Point(4, 22);
            this.VtgPage.Name = "VtgPage";
            this.VtgPage.Padding = new System.Windows.Forms.Padding(3);
            this.VtgPage.Size = new System.Drawing.Size(278, 279);
            this.VtgPage.TabIndex = 5;
            this.VtgPage.Text = "VTG";
            this.VtgPage.UseVisualStyleBackColor = true;
            // 
            // VtgTableLayoutPanel
            // 
            this.VtgTableLayoutPanel.ColumnCount = 2;
            this.VtgTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.VtgTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.VtgTableLayoutPanel.Controls.Add(this.VtgGroundSpeedKnotsLabel, 0, 2);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgPlaceholderTextBox, 1, 0);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgGroundSpeedKtsTextBox, 1, 2);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgModeIndicatorTextBox, 1, 3);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgGroundSpeedKphTextBox, 1, 4);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgTrueCourseTextBox, 1, 5);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgMagneticCourseLabel, 0, 1);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgMagneticCourseTextBox, 1, 1);
            this.VtgTableLayoutPanel.Controls.Add(this.GvtModeIndicatorLabel, 0, 3);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgGroundSpeedKphLabel, 0, 4);
            this.VtgTableLayoutPanel.Controls.Add(this.Checksum, 0, 6);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgTrueCourseLabel, 0, 5);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgChecksumTextBox, 1, 6);
            this.VtgTableLayoutPanel.Controls.Add(this.VtgPlaceholderLabel, 0, 0);
            this.VtgTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VtgTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.VtgTableLayoutPanel.Name = "VtgTableLayoutPanel";
            this.VtgTableLayoutPanel.RowCount = 8;
            this.VtgTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.VtgTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.VtgTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.VtgTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.VtgTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.VtgTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.VtgTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.VtgTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.VtgTableLayoutPanel.Size = new System.Drawing.Size(272, 273);
            this.VtgTableLayoutPanel.TabIndex = 0;
            // 
            // VtgGroundSpeedKnotsLabel
            // 
            this.VtgGroundSpeedKnotsLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.VtgGroundSpeedKnotsLabel.AutoSize = true;
            this.VtgGroundSpeedKnotsLabel.Location = new System.Drawing.Point(3, 56);
            this.VtgGroundSpeedKnotsLabel.Name = "VtgGroundSpeedKnotsLabel";
            this.VtgGroundSpeedKnotsLabel.Size = new System.Drawing.Size(76, 13);
            this.VtgGroundSpeedKnotsLabel.TabIndex = 1;
            this.VtgGroundSpeedKnotsLabel.Text = "Ground Speed";
            // 
            // VtgPlaceholderTextBox
            // 
            this.VtgPlaceholderTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VtgPlaceholderTextBox.Location = new System.Drawing.Point(93, 3);
            this.VtgPlaceholderTextBox.Name = "VtgPlaceholderTextBox";
            this.VtgPlaceholderTextBox.ReadOnly = true;
            this.VtgPlaceholderTextBox.Size = new System.Drawing.Size(176, 20);
            this.VtgPlaceholderTextBox.TabIndex = 5;
            // 
            // VtgGroundSpeedKtsTextBox
            // 
            this.VtgGroundSpeedKtsTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VtgGroundSpeedKtsTextBox.Location = new System.Drawing.Point(93, 53);
            this.VtgGroundSpeedKtsTextBox.Name = "VtgGroundSpeedKtsTextBox";
            this.VtgGroundSpeedKtsTextBox.ReadOnly = true;
            this.VtgGroundSpeedKtsTextBox.Size = new System.Drawing.Size(176, 20);
            this.VtgGroundSpeedKtsTextBox.TabIndex = 6;
            // 
            // VtgModeIndicatorTextBox
            // 
            this.VtgModeIndicatorTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VtgModeIndicatorTextBox.Location = new System.Drawing.Point(93, 78);
            this.VtgModeIndicatorTextBox.Name = "VtgModeIndicatorTextBox";
            this.VtgModeIndicatorTextBox.ReadOnly = true;
            this.VtgModeIndicatorTextBox.Size = new System.Drawing.Size(176, 20);
            this.VtgModeIndicatorTextBox.TabIndex = 7;
            // 
            // VtgGroundSpeedKphTextBox
            // 
            this.VtgGroundSpeedKphTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VtgGroundSpeedKphTextBox.Location = new System.Drawing.Point(93, 103);
            this.VtgGroundSpeedKphTextBox.Name = "VtgGroundSpeedKphTextBox";
            this.VtgGroundSpeedKphTextBox.ReadOnly = true;
            this.VtgGroundSpeedKphTextBox.Size = new System.Drawing.Size(176, 20);
            this.VtgGroundSpeedKphTextBox.TabIndex = 8;
            // 
            // VtgTrueCourseTextBox
            // 
            this.VtgTrueCourseTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VtgTrueCourseTextBox.Location = new System.Drawing.Point(93, 128);
            this.VtgTrueCourseTextBox.Name = "VtgTrueCourseTextBox";
            this.VtgTrueCourseTextBox.ReadOnly = true;
            this.VtgTrueCourseTextBox.Size = new System.Drawing.Size(176, 20);
            this.VtgTrueCourseTextBox.TabIndex = 9;
            // 
            // VtgMagneticCourseLabel
            // 
            this.VtgMagneticCourseLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.VtgMagneticCourseLabel.AutoSize = true;
            this.VtgMagneticCourseLabel.Location = new System.Drawing.Point(3, 31);
            this.VtgMagneticCourseLabel.Name = "VtgMagneticCourseLabel";
            this.VtgMagneticCourseLabel.Size = new System.Drawing.Size(64, 13);
            this.VtgMagneticCourseLabel.TabIndex = 10;
            this.VtgMagneticCourseLabel.Text = "Mag Course";
            // 
            // VtgMagneticCourseTextBox
            // 
            this.VtgMagneticCourseTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VtgMagneticCourseTextBox.Location = new System.Drawing.Point(93, 28);
            this.VtgMagneticCourseTextBox.Name = "VtgMagneticCourseTextBox";
            this.VtgMagneticCourseTextBox.ReadOnly = true;
            this.VtgMagneticCourseTextBox.Size = new System.Drawing.Size(176, 20);
            this.VtgMagneticCourseTextBox.TabIndex = 11;
            // 
            // GvtModeIndicatorLabel
            // 
            this.GvtModeIndicatorLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GvtModeIndicatorLabel.AutoSize = true;
            this.GvtModeIndicatorLabel.Location = new System.Drawing.Point(3, 81);
            this.GvtModeIndicatorLabel.Name = "GvtModeIndicatorLabel";
            this.GvtModeIndicatorLabel.Size = new System.Drawing.Size(78, 13);
            this.GvtModeIndicatorLabel.TabIndex = 3;
            this.GvtModeIndicatorLabel.Text = "Mode Indicator";
            // 
            // VtgGroundSpeedKphLabel
            // 
            this.VtgGroundSpeedKphLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.VtgGroundSpeedKphLabel.AutoSize = true;
            this.VtgGroundSpeedKphLabel.Location = new System.Drawing.Point(3, 106);
            this.VtgGroundSpeedKphLabel.Name = "VtgGroundSpeedKphLabel";
            this.VtgGroundSpeedKphLabel.Size = new System.Drawing.Size(76, 13);
            this.VtgGroundSpeedKphLabel.TabIndex = 2;
            this.VtgGroundSpeedKphLabel.Text = "Ground Speed";
            // 
            // Checksum
            // 
            this.Checksum.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Checksum.AutoSize = true;
            this.Checksum.Location = new System.Drawing.Point(3, 156);
            this.Checksum.Name = "Checksum";
            this.Checksum.Size = new System.Drawing.Size(57, 13);
            this.Checksum.TabIndex = 4;
            this.Checksum.Text = "Checksum";
            // 
            // VtgTrueCourseLabel
            // 
            this.VtgTrueCourseLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.VtgTrueCourseLabel.AutoSize = true;
            this.VtgTrueCourseLabel.Location = new System.Drawing.Point(3, 131);
            this.VtgTrueCourseLabel.Name = "VtgTrueCourseLabel";
            this.VtgTrueCourseLabel.Size = new System.Drawing.Size(65, 13);
            this.VtgTrueCourseLabel.TabIndex = 0;
            this.VtgTrueCourseLabel.Text = "True Course";
            // 
            // VtgChecksumTextBox
            // 
            this.VtgChecksumTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VtgChecksumTextBox.Location = new System.Drawing.Point(93, 153);
            this.VtgChecksumTextBox.Name = "VtgChecksumTextBox";
            this.VtgChecksumTextBox.ReadOnly = true;
            this.VtgChecksumTextBox.Size = new System.Drawing.Size(176, 20);
            this.VtgChecksumTextBox.TabIndex = 12;
            // 
            // VtgPlaceholderLabel
            // 
            this.VtgPlaceholderLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.VtgPlaceholderLabel.AutoSize = true;
            this.VtgPlaceholderLabel.Location = new System.Drawing.Point(3, 6);
            this.VtgPlaceholderLabel.Name = "VtgPlaceholderLabel";
            this.VtgPlaceholderLabel.Size = new System.Drawing.Size(29, 13);
            this.VtgPlaceholderLabel.TabIndex = 13;
            this.VtgPlaceholderLabel.Text = "VTG";
            // 
            // GeneralStatusGroupBox
            // 
            this.GeneralStatusGroupBox.Controls.Add(this.CounterGroupBox);
            this.GeneralStatusGroupBox.Controls.Add(this.GpsPositionGroupBox);
            this.GeneralStatusGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralStatusGroupBox.Location = new System.Drawing.Point(3, 28);
            this.GeneralStatusGroupBox.Name = "GeneralStatusGroupBox";
            this.GeneralStatusGroupBox.Size = new System.Drawing.Size(286, 305);
            this.GeneralStatusGroupBox.TabIndex = 6;
            this.GeneralStatusGroupBox.TabStop = false;
            this.GeneralStatusGroupBox.Text = "General Status";
            // 
            // CounterGroupBox
            // 
            this.CounterGroupBox.Controls.Add(this.CountersTableLayoutPanel);
            this.CounterGroupBox.Location = new System.Drawing.Point(9, 125);
            this.CounterGroupBox.Name = "CounterGroupBox";
            this.CounterGroupBox.Size = new System.Drawing.Size(270, 180);
            this.CounterGroupBox.TabIndex = 3;
            this.CounterGroupBox.TabStop = false;
            this.CounterGroupBox.Text = "Counters";
            // 
            // CountersTableLayoutPanel
            // 
            this.CountersTableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.CountersTableLayoutPanel.ColumnCount = 4;
            this.CountersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.CountersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.CountersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.CountersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.CountersTableLayoutPanel.Controls.Add(this.GgaPacketCounterLabel, 0, 0);
            this.CountersTableLayoutPanel.Controls.Add(this.GgaPacketCounterTextBox, 1, 0);
            this.CountersTableLayoutPanel.Controls.Add(this.GllPacketCounterLable, 0, 1);
            this.CountersTableLayoutPanel.Controls.Add(this.GsaPacketCounterLabel, 0, 2);
            this.CountersTableLayoutPanel.Controls.Add(this.GsvPacketCounterLabel, 0, 3);
            this.CountersTableLayoutPanel.Controls.Add(this.RmcPacketCounterLabel, 0, 4);
            this.CountersTableLayoutPanel.Controls.Add(this.VtgPacketCounterLabel, 0, 5);
            this.CountersTableLayoutPanel.Controls.Add(this.GllPacketCounterTextBox, 1, 1);
            this.CountersTableLayoutPanel.Controls.Add(this.GsaPacketCounterTextBox, 1, 2);
            this.CountersTableLayoutPanel.Controls.Add(this.GsvPacketCounterTextBox, 1, 3);
            this.CountersTableLayoutPanel.Controls.Add(this.RmcPacketCounterTextBox, 1, 4);
            this.CountersTableLayoutPanel.Controls.Add(this.VtgPacketCounterTextBox, 1, 5);
            this.CountersTableLayoutPanel.Controls.Add(this.TotalPacketCounterLabel, 2, 1);
            this.CountersTableLayoutPanel.Controls.Add(this.TotalBytesCounterLabel, 2, 2);
            this.CountersTableLayoutPanel.Controls.Add(this.UnknownPacketCounterLabel, 2, 0);
            this.CountersTableLayoutPanel.Controls.Add(this.CaptureTimeLabel, 2, 3);
            this.CountersTableLayoutPanel.Controls.Add(this.UnknownPacketCounterTextBox, 3, 0);
            this.CountersTableLayoutPanel.Controls.Add(this.ToTalPacketsTextBox, 3, 1);
            this.CountersTableLayoutPanel.Controls.Add(this.TotalBytesTextBox, 3, 2);
            this.CountersTableLayoutPanel.Controls.Add(this.CaptureTimeTextBox, 3, 3);
            this.CountersTableLayoutPanel.Controls.Add(this.ResetStatsButton, 2, 5);
            this.CountersTableLayoutPanel.Controls.Add(this.TimeSinceLastRx, 2, 4);
            this.CountersTableLayoutPanel.Controls.Add(this.TimeSinceLastRxTextBox, 3, 4);
            this.CountersTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CountersTableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.CountersTableLayoutPanel.Name = "CountersTableLayoutPanel";
            this.CountersTableLayoutPanel.RowCount = 6;
            this.CountersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.CountersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.CountersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.CountersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.CountersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.CountersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.CountersTableLayoutPanel.Size = new System.Drawing.Size(264, 161);
            this.CountersTableLayoutPanel.TabIndex = 0;
            // 
            // GgaPacketCounterLabel
            // 
            this.GgaPacketCounterLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GgaPacketCounterLabel.AutoSize = true;
            this.GgaPacketCounterLabel.Location = new System.Drawing.Point(4, 7);
            this.GgaPacketCounterLabel.Name = "GgaPacketCounterLabel";
            this.GgaPacketCounterLabel.Size = new System.Drawing.Size(54, 13);
            this.GgaPacketCounterLabel.TabIndex = 1;
            this.GgaPacketCounterLabel.Text = "GGA Pkts";
            // 
            // GgaPacketCounterTextBox
            // 
            this.GgaPacketCounterTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GgaPacketCounterTextBox.Location = new System.Drawing.Point(75, 4);
            this.GgaPacketCounterTextBox.Name = "GgaPacketCounterTextBox";
            this.GgaPacketCounterTextBox.ReadOnly = true;
            this.GgaPacketCounterTextBox.Size = new System.Drawing.Size(39, 20);
            this.GgaPacketCounterTextBox.TabIndex = 2;
            // 
            // GllPacketCounterLable
            // 
            this.GllPacketCounterLable.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GllPacketCounterLable.AutoSize = true;
            this.GllPacketCounterLable.Location = new System.Drawing.Point(4, 33);
            this.GllPacketCounterLable.Name = "GllPacketCounterLable";
            this.GllPacketCounterLable.Size = new System.Drawing.Size(51, 13);
            this.GllPacketCounterLable.TabIndex = 3;
            this.GllPacketCounterLable.Text = "GLL Pkts";
            // 
            // GsaPacketCounterLabel
            // 
            this.GsaPacketCounterLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsaPacketCounterLabel.AutoSize = true;
            this.GsaPacketCounterLabel.Location = new System.Drawing.Point(4, 59);
            this.GsaPacketCounterLabel.Name = "GsaPacketCounterLabel";
            this.GsaPacketCounterLabel.Size = new System.Drawing.Size(53, 13);
            this.GsaPacketCounterLabel.TabIndex = 4;
            this.GsaPacketCounterLabel.Text = "GSA Pkts";
            // 
            // GsvPacketCounterLabel
            // 
            this.GsvPacketCounterLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GsvPacketCounterLabel.AutoSize = true;
            this.GsvPacketCounterLabel.Location = new System.Drawing.Point(4, 85);
            this.GsvPacketCounterLabel.Name = "GsvPacketCounterLabel";
            this.GsvPacketCounterLabel.Size = new System.Drawing.Size(53, 13);
            this.GsvPacketCounterLabel.TabIndex = 5;
            this.GsvPacketCounterLabel.Text = "GSV Pkts";
            // 
            // RmcPacketCounterLabel
            // 
            this.RmcPacketCounterLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RmcPacketCounterLabel.AutoSize = true;
            this.RmcPacketCounterLabel.Location = new System.Drawing.Point(4, 111);
            this.RmcPacketCounterLabel.Name = "RmcPacketCounterLabel";
            this.RmcPacketCounterLabel.Size = new System.Drawing.Size(55, 13);
            this.RmcPacketCounterLabel.TabIndex = 6;
            this.RmcPacketCounterLabel.Text = "RMC Pkts";
            // 
            // VtgPacketCounterLabel
            // 
            this.VtgPacketCounterLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.VtgPacketCounterLabel.AutoSize = true;
            this.VtgPacketCounterLabel.Location = new System.Drawing.Point(4, 139);
            this.VtgPacketCounterLabel.Name = "VtgPacketCounterLabel";
            this.VtgPacketCounterLabel.Size = new System.Drawing.Size(53, 13);
            this.VtgPacketCounterLabel.TabIndex = 7;
            this.VtgPacketCounterLabel.Text = "VTG Pkts";
            // 
            // GllPacketCounterTextBox
            // 
            this.GllPacketCounterTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GllPacketCounterTextBox.Location = new System.Drawing.Point(75, 30);
            this.GllPacketCounterTextBox.Name = "GllPacketCounterTextBox";
            this.GllPacketCounterTextBox.ReadOnly = true;
            this.GllPacketCounterTextBox.Size = new System.Drawing.Size(39, 20);
            this.GllPacketCounterTextBox.TabIndex = 8;
            // 
            // GsaPacketCounterTextBox
            // 
            this.GsaPacketCounterTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsaPacketCounterTextBox.Location = new System.Drawing.Point(75, 56);
            this.GsaPacketCounterTextBox.Name = "GsaPacketCounterTextBox";
            this.GsaPacketCounterTextBox.ReadOnly = true;
            this.GsaPacketCounterTextBox.Size = new System.Drawing.Size(39, 20);
            this.GsaPacketCounterTextBox.TabIndex = 9;
            // 
            // GsvPacketCounterTextBox
            // 
            this.GsvPacketCounterTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GsvPacketCounterTextBox.Location = new System.Drawing.Point(75, 82);
            this.GsvPacketCounterTextBox.Name = "GsvPacketCounterTextBox";
            this.GsvPacketCounterTextBox.ReadOnly = true;
            this.GsvPacketCounterTextBox.Size = new System.Drawing.Size(39, 20);
            this.GsvPacketCounterTextBox.TabIndex = 10;
            // 
            // RmcPacketCounterTextBox
            // 
            this.RmcPacketCounterTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RmcPacketCounterTextBox.Location = new System.Drawing.Point(75, 108);
            this.RmcPacketCounterTextBox.Name = "RmcPacketCounterTextBox";
            this.RmcPacketCounterTextBox.ReadOnly = true;
            this.RmcPacketCounterTextBox.Size = new System.Drawing.Size(39, 20);
            this.RmcPacketCounterTextBox.TabIndex = 11;
            // 
            // VtgPacketCounterTextBox
            // 
            this.VtgPacketCounterTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VtgPacketCounterTextBox.Location = new System.Drawing.Point(75, 134);
            this.VtgPacketCounterTextBox.Name = "VtgPacketCounterTextBox";
            this.VtgPacketCounterTextBox.ReadOnly = true;
            this.VtgPacketCounterTextBox.Size = new System.Drawing.Size(39, 20);
            this.VtgPacketCounterTextBox.TabIndex = 12;
            // 
            // TotalPacketCounterLabel
            // 
            this.TotalPacketCounterLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TotalPacketCounterLabel.AutoSize = true;
            this.TotalPacketCounterLabel.Location = new System.Drawing.Point(121, 33);
            this.TotalPacketCounterLabel.Name = "TotalPacketCounterLabel";
            this.TotalPacketCounterLabel.Size = new System.Drawing.Size(55, 13);
            this.TotalPacketCounterLabel.TabIndex = 14;
            this.TotalPacketCounterLabel.Text = "Total Pkts";
            // 
            // TotalBytesCounterLabel
            // 
            this.TotalBytesCounterLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TotalBytesCounterLabel.AutoSize = true;
            this.TotalBytesCounterLabel.Location = new System.Drawing.Point(121, 59);
            this.TotalBytesCounterLabel.Name = "TotalBytesCounterLabel";
            this.TotalBytesCounterLabel.Size = new System.Drawing.Size(60, 13);
            this.TotalBytesCounterLabel.TabIndex = 15;
            this.TotalBytesCounterLabel.Text = "Total Bytes";
            // 
            // UnknownPacketCounterLabel
            // 
            this.UnknownPacketCounterLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.UnknownPacketCounterLabel.AutoSize = true;
            this.UnknownPacketCounterLabel.Location = new System.Drawing.Point(121, 7);
            this.UnknownPacketCounterLabel.Name = "UnknownPacketCounterLabel";
            this.UnknownPacketCounterLabel.Size = new System.Drawing.Size(49, 13);
            this.UnknownPacketCounterLabel.TabIndex = 0;
            this.UnknownPacketCounterLabel.Text = "??? Pkts";
            // 
            // CaptureTimeLabel
            // 
            this.CaptureTimeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.CaptureTimeLabel.AutoSize = true;
            this.CaptureTimeLabel.Location = new System.Drawing.Point(121, 85);
            this.CaptureTimeLabel.Name = "CaptureTimeLabel";
            this.CaptureTimeLabel.Size = new System.Drawing.Size(70, 13);
            this.CaptureTimeLabel.TabIndex = 16;
            this.CaptureTimeLabel.Text = "Capture Time";
            // 
            // UnknownPacketCounterTextBox
            // 
            this.UnknownPacketCounterTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UnknownPacketCounterTextBox.Location = new System.Drawing.Point(202, 4);
            this.UnknownPacketCounterTextBox.Name = "UnknownPacketCounterTextBox";
            this.UnknownPacketCounterTextBox.ReadOnly = true;
            this.UnknownPacketCounterTextBox.Size = new System.Drawing.Size(59, 20);
            this.UnknownPacketCounterTextBox.TabIndex = 17;
            // 
            // ToTalPacketsTextBox
            // 
            this.ToTalPacketsTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ToTalPacketsTextBox.Location = new System.Drawing.Point(202, 30);
            this.ToTalPacketsTextBox.Name = "ToTalPacketsTextBox";
            this.ToTalPacketsTextBox.ReadOnly = true;
            this.ToTalPacketsTextBox.Size = new System.Drawing.Size(59, 20);
            this.ToTalPacketsTextBox.TabIndex = 18;
            // 
            // TotalBytesTextBox
            // 
            this.TotalBytesTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TotalBytesTextBox.Location = new System.Drawing.Point(202, 56);
            this.TotalBytesTextBox.Name = "TotalBytesTextBox";
            this.TotalBytesTextBox.ReadOnly = true;
            this.TotalBytesTextBox.Size = new System.Drawing.Size(59, 20);
            this.TotalBytesTextBox.TabIndex = 19;
            // 
            // CaptureTimeTextBox
            // 
            this.CaptureTimeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CaptureTimeTextBox.Location = new System.Drawing.Point(202, 82);
            this.CaptureTimeTextBox.Name = "CaptureTimeTextBox";
            this.CaptureTimeTextBox.ReadOnly = true;
            this.CaptureTimeTextBox.Size = new System.Drawing.Size(59, 20);
            this.CaptureTimeTextBox.TabIndex = 20;
            // 
            // ResetStatsButton
            // 
            this.ResetStatsButton.Location = new System.Drawing.Point(121, 134);
            this.ResetStatsButton.Name = "ResetStatsButton";
            this.ResetStatsButton.Size = new System.Drawing.Size(74, 23);
            this.ResetStatsButton.TabIndex = 21;
            this.ResetStatsButton.Text = "Reset Stats";
            this.ResetStatsButton.UseVisualStyleBackColor = true;
            this.ResetStatsButton.Click += new System.EventHandler(this.ResetStatsButton_Click);
            // 
            // TimeSinceLastRx
            // 
            this.TimeSinceLastRx.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TimeSinceLastRx.AutoSize = true;
            this.TimeSinceLastRx.Location = new System.Drawing.Point(121, 111);
            this.TimeSinceLastRx.Name = "TimeSinceLastRx";
            this.TimeSinceLastRx.Size = new System.Drawing.Size(69, 13);
            this.TimeSinceLastRx.TabIndex = 22;
            this.TimeSinceLastRx.Text = "Time Last Rx";
            // 
            // TimeSinceLastRxTextBox
            // 
            this.TimeSinceLastRxTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimeSinceLastRxTextBox.Location = new System.Drawing.Point(202, 108);
            this.TimeSinceLastRxTextBox.Name = "TimeSinceLastRxTextBox";
            this.TimeSinceLastRxTextBox.ReadOnly = true;
            this.TimeSinceLastRxTextBox.Size = new System.Drawing.Size(59, 20);
            this.TimeSinceLastRxTextBox.TabIndex = 23;
            // 
            // GpsPositionGroupBox
            // 
            this.GpsPositionGroupBox.Controls.Add(this.PositionMapItButton);
            this.GpsPositionGroupBox.Controls.Add(this.PositionSourceSelectLabel);
            this.GpsPositionGroupBox.Controls.Add(this.RmcPositionRadioButton);
            this.GpsPositionGroupBox.Controls.Add(this.GpsPositionTextBox);
            this.GpsPositionGroupBox.Controls.Add(this.GllPositionRadioButton);
            this.GpsPositionGroupBox.Controls.Add(this.GgaPositionRadioButton);
            this.GpsPositionGroupBox.Location = new System.Drawing.Point(9, 19);
            this.GpsPositionGroupBox.Name = "GpsPositionGroupBox";
            this.GpsPositionGroupBox.Size = new System.Drawing.Size(270, 100);
            this.GpsPositionGroupBox.TabIndex = 2;
            this.GpsPositionGroupBox.TabStop = false;
            this.GpsPositionGroupBox.Text = "Position";
            // 
            // PositionMapItButton
            // 
            this.PositionMapItButton.Location = new System.Drawing.Point(209, 70);
            this.PositionMapItButton.Name = "PositionMapItButton";
            this.PositionMapItButton.Size = new System.Drawing.Size(55, 25);
            this.PositionMapItButton.TabIndex = 4;
            this.PositionMapItButton.Text = "Map It!";
            this.PositionMapItButton.UseVisualStyleBackColor = true;
            this.PositionMapItButton.Click += new System.EventHandler(this.PositionMapItButton_Click);
            // 
            // PositionSourceSelectLabel
            // 
            this.PositionSourceSelectLabel.AutoSize = true;
            this.PositionSourceSelectLabel.Location = new System.Drawing.Point(6, 76);
            this.PositionSourceSelectLabel.Name = "PositionSourceSelectLabel";
            this.PositionSourceSelectLabel.Size = new System.Drawing.Size(44, 13);
            this.PositionSourceSelectLabel.TabIndex = 3;
            this.PositionSourceSelectLabel.Text = "Source:";
            // 
            // RmcPositionRadioButton
            // 
            this.RmcPositionRadioButton.AutoSize = true;
            this.RmcPositionRadioButton.Location = new System.Drawing.Point(161, 74);
            this.RmcPositionRadioButton.Name = "RmcPositionRadioButton";
            this.RmcPositionRadioButton.Size = new System.Drawing.Size(49, 17);
            this.RmcPositionRadioButton.TabIndex = 2;
            this.RmcPositionRadioButton.Text = "RMC";
            this.RmcPositionRadioButton.UseVisualStyleBackColor = true;
            // 
            // GpsPositionTextBox
            // 
            this.GpsPositionTextBox.BackColor = System.Drawing.Color.Black;
            this.GpsPositionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GpsPositionTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.GpsPositionTextBox.Location = new System.Drawing.Point(6, 18);
            this.GpsPositionTextBox.Multiline = true;
            this.GpsPositionTextBox.Name = "GpsPositionTextBox";
            this.GpsPositionTextBox.ReadOnly = true;
            this.GpsPositionTextBox.Size = new System.Drawing.Size(258, 50);
            this.GpsPositionTextBox.TabIndex = 1;
            this.GpsPositionTextBox.Text = "80 44\' 27.1234\" / 40 21\' 13.1234\"\r\n80 44.4321\' / 12 23\' 34.1314\"\r\n80.98765 / 23 1" +
    "1\' 54.5234\"";
            // 
            // GllPositionRadioButton
            // 
            this.GllPositionRadioButton.AutoSize = true;
            this.GllPositionRadioButton.Location = new System.Drawing.Point(110, 74);
            this.GllPositionRadioButton.Name = "GllPositionRadioButton";
            this.GllPositionRadioButton.Size = new System.Drawing.Size(45, 17);
            this.GllPositionRadioButton.TabIndex = 1;
            this.GllPositionRadioButton.Text = "GLL";
            this.GllPositionRadioButton.UseVisualStyleBackColor = true;
            // 
            // GgaPositionRadioButton
            // 
            this.GgaPositionRadioButton.AutoSize = true;
            this.GgaPositionRadioButton.Checked = true;
            this.GgaPositionRadioButton.Location = new System.Drawing.Point(56, 74);
            this.GgaPositionRadioButton.Name = "GgaPositionRadioButton";
            this.GgaPositionRadioButton.Size = new System.Drawing.Size(48, 17);
            this.GgaPositionRadioButton.TabIndex = 0;
            this.GgaPositionRadioButton.TabStop = true;
            this.GgaPositionRadioButton.Text = "GGA";
            this.GgaPositionRadioButton.UseVisualStyleBackColor = true;
            // 
            // GuiTimer
            // 
            this.GuiTimer.Tick += new System.EventHandler(this.GuiTimer_Tick);
            // 
            // TheForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.MainTableLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(600, 400);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "TheForm";
            this.Text = "NMEA Test Application";
            this.MainTableLayoutPanel.ResumeLayout(false);
            this.MainTableLayoutPanel.PerformLayout();
            this.MainStatusStrip.ResumeLayout(false);
            this.MainStatusStrip.PerformLayout();
            this.MainToolStrip.ResumeLayout(false);
            this.MainToolStrip.PerformLayout();
            this.NmeaSentenceTabControl.ResumeLayout(false);
            this.GgaPage.ResumeLayout(false);
            this.GgaTableLayoutPanel.ResumeLayout(false);
            this.GgaTableLayoutPanel.PerformLayout();
            this.GllPage.ResumeLayout(false);
            this.GllTableLayoutPanel.ResumeLayout(false);
            this.GllTableLayoutPanel.PerformLayout();
            this.GsaPage.ResumeLayout(false);
            this.GsaTableLayoutPanel.ResumeLayout(false);
            this.GsaTableLayoutPanel.PerformLayout();
            this.GsvPage.ResumeLayout(false);
            this.GsvTableLayoutPanel.ResumeLayout(false);
            this.GsvDataTableLayoutPanel.ResumeLayout(false);
            this.GsvDataTableLayoutPanel.PerformLayout();
            this.GsvSatellitesTableLayoutPanel.ResumeLayout(false);
            this.GsvSatellitesTableLayoutPanel.PerformLayout();
            this.RmcPage.ResumeLayout(false);
            this.RmcTableLayoutPanel.ResumeLayout(false);
            this.RmcTableLayoutPanel.PerformLayout();
            this.VtgPage.ResumeLayout(false);
            this.VtgTableLayoutPanel.ResumeLayout(false);
            this.VtgTableLayoutPanel.PerformLayout();
            this.GeneralStatusGroupBox.ResumeLayout(false);
            this.CounterGroupBox.ResumeLayout(false);
            this.CountersTableLayoutPanel.ResumeLayout(false);
            this.CountersTableLayoutPanel.PerformLayout();
            this.GpsPositionGroupBox.ResumeLayout(false);
            this.GpsPositionGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.IO.Ports.SerialPort MySerialPort;
        private System.Windows.Forms.TableLayoutPanel MainTableLayoutPanel;
        private System.Windows.Forms.TabControl NmeaSentenceTabControl;
        private System.Windows.Forms.TabPage GgaPage;
        private System.Windows.Forms.TabPage GllPage;
        private System.Windows.Forms.TabPage GsaPage;
        private System.Windows.Forms.TabPage GsvPage;
        private System.Windows.Forms.TabPage RmcPage;
        private System.Windows.Forms.TabPage VtgPage;
        private System.Windows.Forms.ToolStrip MainToolStrip;
        private System.Windows.Forms.ToolStripDropDownButton SerialPortSelectButton;
        private System.Windows.Forms.TableLayoutPanel GgaTableLayoutPanel;
        private System.Windows.Forms.Label GgaUtcTimeLabel;
        private System.Windows.Forms.TextBox UtcTimeTextBox;
        private System.Windows.Forms.Label GgaPositionLabel;
        private System.Windows.Forms.TextBox PositionTextBox;
        private System.Windows.Forms.Label GgaGpsQualityLabel;
        private System.Windows.Forms.Label GgaSatellitesUsedLabel;
        private System.Windows.Forms.Label GgaHdopLabel;
        private System.Windows.Forms.Label GgaAltitudeLabel;
        private System.Windows.Forms.TextBox GpsQualityTextBox;
        private System.Windows.Forms.TextBox SatellitesUsedTextBox;
        private System.Windows.Forms.TextBox HorizontalDopTextBox;
        private System.Windows.Forms.TextBox AltitudeTextBox;
        private System.Windows.Forms.TextBox ChecksumTextBox;
        private System.Windows.Forms.Label GgaChecksumLabel;
        private System.Windows.Forms.Timer GuiTimer;
        private System.Windows.Forms.StatusStrip MainStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel SerialPortStatusLabel;
        private System.Windows.Forms.GroupBox GeneralStatusGroupBox;
        private System.Windows.Forms.TextBox GpsPositionTextBox;
        private System.Windows.Forms.Label UnknownPacketCounterLabel;
        private System.Windows.Forms.TableLayoutPanel GsaTableLayoutPanel;
        private System.Windows.Forms.Label GsaFixModeLabel;
        private System.Windows.Forms.Label GsaFixTypeLabel;
        private System.Windows.Forms.Label GsaSatellitesUsedLabel;
        private System.Windows.Forms.Label GsaPositionDopLabel;
        private System.Windows.Forms.Label GsaHorizontalDopLabel;
        private System.Windows.Forms.Label GsaVerticalDopLabel;
        private System.Windows.Forms.Label GsaChecksumLabel;
        private System.Windows.Forms.TextBox GsaFixModeTextBox;
        private System.Windows.Forms.TextBox GsaFixTypeTextBox;
        private System.Windows.Forms.TextBox GsaSatellitesUsedTextBox;
        private System.Windows.Forms.TextBox GsaPositionDopTextBox;
        private System.Windows.Forms.TextBox GsaHorizontalDopTextBox;
        private System.Windows.Forms.TextBox GsaVerticalDopTextBox;
        private System.Windows.Forms.TextBox GsaChecksumTextBox;
        private System.Windows.Forms.TableLayoutPanel GllTableLayoutPanel;
        private System.Windows.Forms.Label PositionLabel;
        private System.Windows.Forms.Label GllUtcLabel;
        private System.Windows.Forms.Label GllStatusLabel;
        private System.Windows.Forms.Label GllModeIndicatorLabel;
        private System.Windows.Forms.Label GllChecksumLabel;
        private System.Windows.Forms.TextBox GllStatusTextBox;
        private System.Windows.Forms.TextBox GllModeIndicatorTextBox;
        private System.Windows.Forms.TextBox GllChecksumTextBox;
        private System.Windows.Forms.GroupBox GpsPositionGroupBox;
        private System.Windows.Forms.RadioButton RmcPositionRadioButton;
        private System.Windows.Forms.RadioButton GllPositionRadioButton;
        private System.Windows.Forms.RadioButton GgaPositionRadioButton;
        private System.Windows.Forms.GroupBox CounterGroupBox;
        private System.Windows.Forms.TableLayoutPanel CountersTableLayoutPanel;
        private System.Windows.Forms.Label GgaPacketCounterLabel;
        private System.Windows.Forms.TextBox GgaPacketCounterTextBox;
        private System.Windows.Forms.Label GllPacketCounterLable;
        private System.Windows.Forms.Label GsaPacketCounterLabel;
        private System.Windows.Forms.Label GsvPacketCounterLabel;
        private System.Windows.Forms.Label RmcPacketCounterLabel;
        private System.Windows.Forms.Label VtgPacketCounterLabel;
        private System.Windows.Forms.TextBox GllPacketCounterTextBox;
        private System.Windows.Forms.TextBox GsaPacketCounterTextBox;
        private System.Windows.Forms.TextBox GsvPacketCounterTextBox;
        private System.Windows.Forms.TextBox RmcPacketCounterTextBox;
        private System.Windows.Forms.TextBox VtgPacketCounterTextBox;
        private System.Windows.Forms.Label TotalPacketCounterLabel;
        private System.Windows.Forms.Label TotalBytesCounterLabel;
        private System.Windows.Forms.Label CaptureTimeLabel;
        private System.Windows.Forms.TableLayoutPanel RmcTableLayoutPanel;
        private System.Windows.Forms.Label RmcUtcTimeLabel;
        private System.Windows.Forms.Label RmcStatusLabel;
        private System.Windows.Forms.Label RmcPositionLabel;
        private System.Windows.Forms.Label RmcGroundSpeedLabel;
        private System.Windows.Forms.Label RcmTrackAngleLabel;
        private System.Windows.Forms.Label RmcModeIndicatorLabel;
        private System.Windows.Forms.Label RmcChecksumLabel;
        private System.Windows.Forms.TextBox RmcUtcDateTimeTextBox;
        private System.Windows.Forms.TextBox RmcPositionTextBox;
        private System.Windows.Forms.TextBox RmcStatusTextBox;
        private System.Windows.Forms.TextBox RmcModeIndicatorTextBox;
        private System.Windows.Forms.TextBox RmcGroundSpeedTextBox;
        private System.Windows.Forms.TextBox RmcTrackAngleTextBox;
        private System.Windows.Forms.TextBox RmcChecksumTextBox;
        private System.Windows.Forms.TextBox GllUtcTimeTextBox;
        private System.Windows.Forms.TextBox GllPositionTextBox;
        private System.Windows.Forms.TableLayoutPanel VtgTableLayoutPanel;
        private System.Windows.Forms.Label VtgTrueCourseLabel;
        private System.Windows.Forms.Label VtgGroundSpeedKnotsLabel;
        private System.Windows.Forms.Label VtgGroundSpeedKphLabel;
        private System.Windows.Forms.Label GvtModeIndicatorLabel;
        private System.Windows.Forms.Label Checksum;
        private System.Windows.Forms.TextBox VtgPlaceholderTextBox;
        private System.Windows.Forms.TextBox VtgGroundSpeedKtsTextBox;
        private System.Windows.Forms.TextBox VtgModeIndicatorTextBox;
        private System.Windows.Forms.TextBox VtgGroundSpeedKphTextBox;
        private System.Windows.Forms.TextBox VtgTrueCourseTextBox;
        private System.Windows.Forms.Label VtgMagneticCourseLabel;
        private System.Windows.Forms.TextBox VtgMagneticCourseTextBox;
        private System.Windows.Forms.TextBox VtgChecksumTextBox;
        private System.Windows.Forms.Label VtgPlaceholderLabel;
        private System.Windows.Forms.TableLayoutPanel GsvTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel GsvSatellitesTableLayoutPanel;
        private System.Windows.Forms.Label GsvSatelliteLabel1;
        private System.Windows.Forms.Label GsvSatelliteLabel2;
        private System.Windows.Forms.Label GsvSatelliteLabel3;
        private System.Windows.Forms.Label GsvSatelliteLabel4;
        private System.Windows.Forms.Label GsvSatelliteLabel5;
        private System.Windows.Forms.Label GsvSatelliteLabel6;
        private System.Windows.Forms.Label GsvSatelliteLabel7;
        private System.Windows.Forms.Label GsvSatelliteLabel8;
        private System.Windows.Forms.Label GsvSatelliteLabel9;
        private System.Windows.Forms.Label GsvSatelliteLabel10;
        private System.Windows.Forms.Label GsvSatelliteLabel11;
        private System.Windows.Forms.Label GsvSatelliteLabel12;
        private System.Windows.Forms.Label PositionSourceSelectLabel;
        private System.Windows.Forms.TextBox UnknownPacketCounterTextBox;
        private System.Windows.Forms.TextBox ToTalPacketsTextBox;
        private System.Windows.Forms.TextBox TotalBytesTextBox;
        private System.Windows.Forms.TextBox CaptureTimeTextBox;
        private System.Windows.Forms.Button ResetStatsButton;
        private System.Windows.Forms.Label TimeSinceLastRx;
        private System.Windows.Forms.TextBox TimeSinceLastRxTextBox;
        private System.Windows.Forms.TableLayoutPanel GsvDataTableLayoutPanel;
        private System.Windows.Forms.Label GsvTotalMessagesLabel;
        private System.Windows.Forms.Label GsvSequenceNumberLabel;
        private System.Windows.Forms.Label GsvSatellitesInViewLabel;
        private System.Windows.Forms.Label GsvIdLabel;
        private System.Windows.Forms.Label GsvElevationLabel;
        private System.Windows.Forms.Label GsvAzimuthLabel;
        private System.Windows.Forms.Label GsvSnrLabel;
        private System.Windows.Forms.Label GsvSatelliteIndex;
        private System.Windows.Forms.Label GsvMessageCountText;
        private System.Windows.Forms.Label GsvSequenceNumberText;
        private System.Windows.Forms.Label GsvSatellitesNumberText;
        private System.Windows.Forms.Button PositionMapItButton;
        private System.Windows.Forms.ToolStripButton SerialMonitorButton;
    }
}

